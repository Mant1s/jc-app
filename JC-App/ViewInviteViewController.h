//
//  ViewInviteViewController.h
//  JC-App
//
//  Created by Frans Lourens on 2014/05/05.
//  Copyright (c) 2014 automytest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <Parse/Parse.h>

@interface ViewInviteViewController : UIViewController <MKMapViewDelegate>
{
    NSNumber *totalCount;
    NSNumber *totalAcceptedCounts;
}

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *reasonLabel;
@property (weak, nonatomic) IBOutlet UILabel *topicLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (nonatomic,strong) PFObject *invite;
@property (nonatomic,strong) PFUser *iUser;
@property (weak, nonatomic) IBOutlet UILabel *meetingSpotLabel;
@property (weak, nonatomic) IBOutlet UILabel *acceptLabel;
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
@property (weak, nonatomic) IBOutlet UILabel *total;
- (void) getPushNotification:(PFObject *)aInvite;
@property (weak, nonatomic) IBOutlet UILabel *totalAccepted;
@property (weak, nonatomic) IBOutlet UILabel *totalAcceptedCount;
- (IBAction)confirmAction:(id)sender;
//- (IBAction)segmentAction:(id)sender;
@end
