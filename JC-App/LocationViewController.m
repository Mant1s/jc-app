#import <Parse/Parse.h>

#import "LocationViewController.h"
#import "ViewController.h"
#import "InviteViewController.h"

#import "CircleOverlay.h"
#import "GeoPointAnnotation.h"
#import "GeoQueryAnnotation.h"
#import "SPGooglePlacesAutocompleteQuery.h"
#import "SPGooglePlacesAutocompletePlace.h"
#import "InviteViewController.h"

#define METERS_PER_MILE 1609.344
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define MAX_LENGTH 20

enum PinAnnotationTypeTag {
    PinAnnotationTypeTagGeoPoint = 0,
    PinAnnotationTypeTagGeoQuery = 1
};

@interface LocationViewController ()

@end

@implementation LocationViewController
@synthesize mapView;
@synthesize location;
@synthesize locationManager = _locationManager;
UIPickerView *distancePicker;
NSString *distanceText;
BOOL cdLiesInsideInterval;
BOOL cbtLiesInsideInterval;
BOOL cetLiesInsideInterval;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self setInitialLocation:self.locationManager.location];
        searchQuery = [[SPGooglePlacesAutocompleteQuery alloc] init];
        searchQuery.radius = 4000;
        shouldBeginEditing = YES;
    }

    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.searchBar sizeToFit];
    self.totalUsers.userInteractionEnabled = NO;
    self.warning.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
    self.latDelta = 0.08+0.15;
    self.lonDelta = 0.08+0.15;
    self.radius = 4000;
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 75, 20)];
    [button addTarget:self action:@selector(nextController:) forControlEvents:UIControlEventTouchUpInside];
    button.titleLabel.font= [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
    [button setTitle:@"Next" forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor clearColor]];
    [button setBounds:CGRectMake(0, 0, 75, 20)];
    [button setImage:[UIImage imageNamed:@"UIButtonBarArrowRight.png"] forState:UIControlStateNormal];
    [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [button setImageEdgeInsets:UIEdgeInsetsMake(0, CGRectGetWidth(button.frame)-15, 0, 0)];

    UIBarButtonItem * nextButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = nextButtonItem;
    
    //self.navigationItem.rightBarButtonItem = nextButtonItem;

    self.navigationItem.rightBarButtonItem.tintColor = [UIColor redColor];
    
    self.searchBar.delegate = self;
    
    distanceChoices = [[NSArray alloc]
                       initWithObjects:
                       @"2 KM", @"4 KM", @"6 KM", @"8 KM", @"10 KM",
                       @"12 KM", @"14 KM", @"16 KM", @"18 KM", @"20 KM",
                       @"22 KM", @"24 KM", @"26 KM", @"28 KM", @"30 KM",
                       @"32 KM", @"34 KM", @"36 KM", @"38 KM", @"40 KM",
                       @"42 KM", @"44 KM", @"46 KM", @"48 KM", @"50 KM",
                       nil];
    
    distancePicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
    distancePicker.delegate = self;
    distancePicker.dataSource = self;
    [distancePicker setShowsSelectionIndicator:YES];
    distanceField.inputView = distancePicker;
    [distancePicker selectRow:1 inComponent:0 animated:YES];

    UIToolbar*  distanceToolbar = [[UIToolbar alloc]
                                   initWithFrame:CGRectMake(0, 0, 320, 40)];
    [distanceToolbar sizeToFit];
    
    NSMutableArray *distanceBarItems = [[NSMutableArray alloc] init];
    UIBarButtonItem *distanceFlexSpace = [[UIBarButtonItem alloc]
                                          initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                          target:self
                                          action:nil];
    
    [distanceBarItems addObject:distanceFlexSpace];
    UIBarButtonItem *distanceDoneBtn = [[UIBarButtonItem alloc]
                                        initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                        target:self
                                        action:@selector(distanceDoneClicked)];
    
    [distanceBarItems addObject:distanceDoneBtn];
    [distanceToolbar setItems:distanceBarItems animated:YES];
    
    distanceField.inputAccessoryView = distanceToolbar;
    
    if (!self.location) {
        self.mapView.region = MKCoordinateRegionMake(self.location.coordinate,
                                                     MKCoordinateSpanMake(self.latDelta, self.lonDelta));
        [self setAddress:self.mapView.userLocation.location];
        
        CircleOverlay *overlay = [[CircleOverlay alloc] initWithCoordinate:self.mapView.userLocation.coordinate radius:self.radius];
        [self.mapView addOverlay:overlay];
        
        GeoQueryAnnotation *annotation = [[GeoQueryAnnotation alloc]
                                          initWithCoordinate:self.mapView.userLocation.coordinate
                                          radius:self.radius];
        
        [self.mapView addAnnotation:annotation];
        [self updateLocations:self.mapView.userLocation.location];
    }
    
    self.searchDisplayController.searchBar.placeholder = @"Type your address";
    
    self.meetingSpotField.delegate = self;
    [self.meetingSpotField.layer setBorderWidth:1.00];
    [self.meetingSpotField.layer setBorderColor:[[
            [UIColor lightGrayColor] colorWithAlphaComponent:0.5]
            CGColor
    ]];
}

-(void)distanceDoneClicked
{
    if (distanceText == nil)
    {
        distanceField.text = distanceChoices[0];
        [self setRadius:[distanceText doubleValue]*1000];
        [self updateCircleRadius:[distanceText intValue]];
 
    }
    else {
        distanceField.text = distanceText;
        [self setRadius:[distanceText doubleValue]*1000];
        [self updateCircleRadius:[distanceText intValue]];
        
    }
    
    [distanceField resignFirstResponder];
}

-(void)updateCircleRadius:(int)radius
{
    float circle = (radius / 100.0f) + 0.15;
    MKCoordinateSpan span;
    
    span.latitudeDelta = circle;
    span.longitudeDelta = circle;
    
    MKCoordinateRegion region;
    region.span = span;
    region.center = self.location.coordinate;
    
    [self.mapView setRegion:region animated:YES];
    [self updateLocations:self.location];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView
            numberOfRowsInComponent:(NSInteger)component
{
        return distanceChoices.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [distanceChoices objectAtIndex:row];
}
- (void) pickerView:(UIPickerView *)pickerView
       didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
        distanceText = (NSString *)[distanceChoices objectAtIndex:row];
}

- (void)viewDidUnload {
    [self setMapView:nil];
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [searchResultPlaces count];
}

- (SPGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSIndexPath *)indexPath {
    return searchResultPlaces[indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"SPGooglePlacesAutocompleteCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"GillSans" size:16.0];
    cell.textLabel.text = [self placeAtIndexPath:indexPath].name;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.searchDisplayController setActive:NO animated:YES];
    SPGooglePlacesAutocompletePlace *place = [self placeAtIndexPath:indexPath];
    
    [place resolveToPlacemark:^(CLPlacemark *placemark, NSString *addressString, NSError *error) {
        
        if (error) {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Could not map selected Place"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil, nil];
            [alert show];
        } else if (placemark) {
            [self setInitialLocation:placemark.location];
            [self recenterMapToPlacemark:placemark];
            [self dismissSearchControllerWhileNotStayingActive];
            [self.searchDisplayController.searchResultsTableView deselectRowAtIndexPath:indexPath animated:NO];
            [self setAddress:placemark.location];
        }
    }];
    
    
}

#pragma mark -
#pragma mark UISearchDisplayDelegate

- (void)handleSearchForSearchString:(NSString *)searchString {
    searchQuery.location = self.mapView.userLocation.coordinate;
    searchQuery.input = searchString;
    [searchQuery fetchPlaces:^(NSArray *places, NSError *error) {
        if (error) {
            SPPresentAlertViewWithErrorAndTitle(error, @"Could not fetch Places");
        } else {
            searchResultPlaces = places;
            [self.searchDisplayController.searchResultsTableView reloadData];
        }
    }];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString {
    [self handleSearchForSearchString:searchString];
    return YES;
}

#pragma mark -
#pragma mark UISearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
    if (![searchBar isFirstResponder]) {
        shouldBeginEditing = NO;
        [self.mapView removeAnnotation:selectedPlaceAnnotation];
    }
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
    if (shouldBeginEditing) {
        [self.searchDisplayController setActive:YES animated:NO];
        self.searchDisplayController.searchBar.text = nil;
        self.searchBar.showsScopeBar = NO;
        searchBar.delegate = (id <UISearchBarDelegate>)self.searchDisplayController;
        [self.searchDisplayController.searchBar setShowsCancelButton:YES animated:NO];
    }
    BOOL boolToReturn = shouldBeginEditing;
    shouldBeginEditing = YES;
    return boolToReturn;
}

- (void)dismissSearchControllerWhileNotStayingActive {
    [self.searchDisplayController.searchBar setShowsCancelButton:NO animated:NO];
    [self.searchDisplayController.searchBar resignFirstResponder];
    [self.searchDisplayController setActive:NO animated:NO];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void) searchDisplayControllerDidBeginSearch:(UISearchDisplayController
                                                *)controller {
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void) searchDisplayControllerWillEndSearch:(UISearchDisplayController
                                               *)controller {
    [self.searchBar resignFirstResponder];
    self.searchBar.delegate = self;
    [self.searchDisplayController setActive:NO animated:NO];
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapViewIn
            viewForAnnotation:(id <MKAnnotation>)annotation
{
    if (    mapViewIn != self.mapView ||
        [annotation isKindOfClass:[MKUserLocation class]]
        )
    {
        return nil;
    }
    
    static NSString *GeoQueryAnnotationIdentifier = @"SPGooglePlacesAutocompleteAnnotation";
    static NSString *GeoPointAnnotationIdentifier = @"PurplePinAnnotation";
    
    if ([annotation isKindOfClass:[GeoQueryAnnotation class]]) {
        MKPinAnnotationView *annotationView =
        (MKPinAnnotationView *)[mapView
                                dequeueReusableAnnotationViewWithIdentifier:GeoQueryAnnotationIdentifier];
        
        if (!annotationView) {
            annotationView = [[MKPinAnnotationView alloc]
                              initWithAnnotation:annotation
                              reuseIdentifier:GeoQueryAnnotationIdentifier];
            annotationView.tag = PinAnnotationTypeTagGeoQuery;
            annotationView.canShowCallout = NO;
            annotationView.pinColor = MKPinAnnotationColorRed;
            annotationView.animatesDrop = NO;
            annotationView.draggable = YES;
        }
        
        return annotationView;
    } else if ([annotation isKindOfClass:[GeoPointAnnotation class]]) {
        MKPinAnnotationView *annotationView =
        (MKPinAnnotationView *)[mapView
                                dequeueReusableAnnotationViewWithIdentifier:GeoPointAnnotationIdentifier];
        
        if (!annotationView) {
            annotationView = [[MKPinAnnotationView alloc]
                              initWithAnnotation:annotation
                              reuseIdentifier:GeoPointAnnotationIdentifier];
            annotationView.tag = PinAnnotationTypeTagGeoPoint;
            annotationView.canShowCallout = NO;
            annotationView.pinColor = MKPinAnnotationColorPurple;
            annotationView.animatesDrop = NO;
            annotationView.draggable = NO;
        }
        
        return annotationView;
    }
    
    return nil;
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay {
    static NSString *CircleOverlayIdentifier = @"Circle";
    
    if ([overlay isKindOfClass:[CircleOverlay class]]) {
        CircleOverlay *circleOverlay = (CircleOverlay *)overlay;
        
        MKCircleView *annotationView =
        (MKCircleView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:CircleOverlayIdentifier];
        
        if (!annotationView) {
            MKCircle *circle = [MKCircle
                                circleWithCenterCoordinate:circleOverlay.coordinate
                                radius:circleOverlay.radius];
            annotationView = [[MKCircleView alloc] initWithCircle:circle];
        }
        
        if (overlay == self.targetOverlay) {
            annotationView.fillColor = [UIColor colorWithRed:1.0f green:0.0f blue:0.0f alpha:0.3f];
            annotationView.strokeColor = [UIColor redColor];
            annotationView.lineWidth = 1.0f;
        } else {
            annotationView.fillColor = [UIColor colorWithWhite:0.3f alpha:0.3f];
            annotationView.strokeColor = [UIColor purpleColor];
            annotationView.lineWidth = 2.0f;
        }
        
        return annotationView;
    }
    
    return nil;
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    [self.mapView selectAnnotation:selectedPlaceAnnotation animated:YES];
}

- (void)mapView:(MKMapView *)mapView
 annotationView:(MKAnnotationView *)annotationView
didChangeDragState:(MKAnnotationViewDragState)newState
   fromOldState:(MKAnnotationViewDragState)oldState
{
    if (newState == MKAnnotationViewDragStateEnding) {
        CLLocationCoordinate2D droppedAt = annotationView.annotation.coordinate;
        
        
        CLLocation *pinLocation = [ [CLLocation alloc]
                                initWithLatitude:droppedAt.latitude
                                longitude:droppedAt.longitude];
        location = pinLocation;
        [self setInitialLocation:pinLocation];
        [self setAddress:pinLocation];
    }
}

- (void)mapView:(MKMapView *)mapView
didUpdateUserLocation:(MKUserLocation *)userLocation
{
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView removeOverlays:self.mapView.overlays];
    self.mapView.region = MKCoordinateRegionMake(userLocation.location.coordinate,
                                                 MKCoordinateSpanMake(self.latDelta, self.lonDelta));
    [self setInitialLocation:userLocation.location];
    [self configureOverlay];
    [self updateLocations:userLocation.location];
}

- (void)recenterMapToPlacemark:(CLPlacemark *)placemark {
    location = placemark.location;
    self.mapView.region = MKCoordinateRegionMake(placemark.location.coordinate,
                                                 MKCoordinateSpanMake((self.radius/1000)/100.0f+0.15f, (self.radius/1000)/100.0f+0.15f));
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView removeOverlays:self.mapView.overlays];
    [self.mapView setShowsUserLocation:NO];
    
    CircleOverlay *overlay = [[CircleOverlay alloc] initWithCoordinate:placemark.location.coordinate radius:self.radius];
    [self.mapView addOverlay:overlay];
    
    GeoQueryAnnotation *annotation = [[GeoQueryAnnotation alloc]
                                      initWithCoordinate:placemark.location.coordinate
                                      radius:self.radius];
    
    [self.mapView addAnnotation:annotation];
}

- (IBAction)recenterMapToUserLocation:(id)sender {
    location = self.mapView.userLocation.location;
    self.mapView.region = MKCoordinateRegionMake(self.mapView.userLocation.coordinate,
                                                 MKCoordinateSpanMake(self.latDelta, self.lonDelta));
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView removeOverlays:self.mapView.overlays];
    [self.mapView setShowsUserLocation:NO];
    [self setAddress:self.mapView.userLocation.location];
    
    CircleOverlay *overlay = [[CircleOverlay alloc] initWithCoordinate:self.mapView.userLocation.coordinate radius:self.radius];
    [self.mapView addOverlay:overlay];
    
    GeoQueryAnnotation *annotation = [[GeoQueryAnnotation alloc]
                                      initWithCoordinate:self.mapView.userLocation.coordinate
                                      radius:self.radius];
    
    [self.mapView addAnnotation:annotation];
}

- (void)setInitialLocation:(CLLocation *)aLocation {
    self.location = aLocation;
}

- (void)setAddress:(CLLocation *)aLocation {
    
    if (!self.location) {
        [self setInitialLocation:aLocation];
    }
    
    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
    
    [geoCoder reverseGeocodeLocation:aLocation completionHandler:
     ^(NSArray *placemarks, NSError *error) {
         for (CLPlacemark * placemark in placemarks) {
             self.country = [placemark country];
             self.city = [placemark locality];
             self.latitude = aLocation.coordinate.latitude;
             self.longitude = aLocation.coordinate.longitude;
             
             NSString * address = [placemark name];
             self.searchDisplayController.searchBar.text = address;
             [self updateLocations:aLocation];
         }
     }];
}

- (void)updateLocations:(CLLocation *)aLocation {
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView removeOverlays:self.mapView.overlays];
    
    CGFloat kilometers = self.radius/1000.0f;
    PFGeoPoint *point = [PFGeoPoint geoPointWithLatitude:aLocation.coordinate.latitude longitude:aLocation.coordinate.longitude];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *today = [NSDate date];
    
    NSDateFormatter *myFormatter = [[NSDateFormatter alloc] init];
    NSDateFormatter *myTimeFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"EEEE"];
    [myTimeFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [myTimeFormatter setDateFormat:@"HH:mm"];
    
    NSString *dayOfWeek = [myFormatter stringFromDate:today];
    
    PFQuery *query = [PFUser query];
    [query whereKey:@"objectId" notEqualTo:[PFUser currentUser].objectId];
    [query whereKey:@"geolocation"
       nearGeoPoint:[PFGeoPoint geoPointWithLatitude:aLocation.coordinate.latitude
                                           longitude:aLocation.coordinate.longitude]
    withinKilometers:kilometers];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSMutableArray *mutableArray = [NSMutableArray array];
            NSUInteger totalObjects = [objects count];
            if (totalObjects > 0) {
            for (PFObject *object in objects) {
                
                PFQuery *weekQuery = [PFQuery queryWithClassName:@"notification"];
                [weekQuery whereKey:@"user" equalTo:object];
                [weekQuery getFirstObjectInBackgroundWithBlock:^(PFObject *week, NSError *error) {
                    if (!error) {
                        
                        NSDateComponents *beginTime = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:[myTimeFormatter dateFromString:week[@"beginTime"]]];
                        
                        NSDateComponents *endTime = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:[myTimeFormatter dateFromString:week[@"endTime"]]];
                        
                        NSDateComponents *todayComps = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:today];
                        
                        beginTime.day = todayComps.day;
                        beginTime.month = todayComps.month;
                        beginTime.year = todayComps.year;
                        NSDate *todayAtMidnight = [calendar dateFromComponents:beginTime];
                        
                        endTime.day = todayComps.day;
                        endTime.month = todayComps.month;
                        endTime.year = todayComps.year;
                        NSDate *laterAtMidnight = [calendar dateFromComponents:endTime];
    
                        if ([dayOfWeek isEqualToString:@"Monday"])
                            cdLiesInsideInterval = [week[@"Monday"] boolValue];
                        else if ([dayOfWeek isEqualToString:@"Tuesday"])
                            cdLiesInsideInterval = [week[@"Tuesday"] boolValue];
                        else if ([dayOfWeek isEqualToString:@"Wednesday"])
                            cdLiesInsideInterval = [week[@"Wednesday"] boolValue];
                        else if ([dayOfWeek isEqualToString:@"Thursday"])
                            cdLiesInsideInterval = [week[@"Thursday"] boolValue];
                        else if ([dayOfWeek isEqualToString:@"Friday"])
                            cdLiesInsideInterval = [week[@"Friday"] boolValue];
                        else if ([dayOfWeek isEqualToString:@"Saturday"])
                            cdLiesInsideInterval = [week[@"Saturday"] boolValue];
                        else if ([dayOfWeek isEqualToString:@"Sunday"])
                            cdLiesInsideInterval = [week[@"Sunday"] boolValue];
                        else
                            cdLiesInsideInterval = NO;
                        
                        if (cdLiesInsideInterval) {
                    
                            NSComparisonResult beginResult = [today compare:todayAtMidnight]; // comparing two dates
                            NSComparisonResult endResult = [today compare:laterAtMidnight]; // comparing two dates

                            if(beginResult == NSOrderedAscending)
                            {
                                cbtLiesInsideInterval = NO;
                            }
                            else if(beginResult == NSOrderedDescending)
                            {
                                cbtLiesInsideInterval = YES;
                            }
                            else
                            {
                                cbtLiesInsideInterval = YES;
                            }
                        
                            
                            if(endResult == NSOrderedAscending)
                            {
                                cetLiesInsideInterval = YES;
                            }
                            else if(endResult == NSOrderedDescending)
                            {
                                cetLiesInsideInterval = NO;
                            }
                            else
                            {
                                cetLiesInsideInterval = YES;
                            }
                            
                            if (cbtLiesInsideInterval && cetLiesInsideInterval) {
                                
                                PFGeoPoint *objectPoint = object[@"geolocation"];
                                
                                if (objectPoint.latitude == point.latitude && objectPoint.longitude == point.longitude) {
                                
                                } else {
                                    
                                    GeoPointAnnotation *geoPointAnnotation = [[GeoPointAnnotation alloc]
                                                                              initWithObject:object];
                                    [self.mapView addAnnotation:geoPointAnnotation];
                                    
                                    
                                    [mutableArray addObject:object];
                                    
                                    people = mutableArray;
                                    
                                }
                            }
                        }
                    }
                    else if (error.code == 101) {

                    }
                    
                    NSUInteger i = [people count];
                    
                    if (i > 0) {
                    
                    self.totalUsers.text = [NSString stringWithFormat:@"%lu", (unsigned long)i, nil];
                    } else {
                        people = nil;
                        self.totalUsers.text = [NSString stringWithFormat:@"%u", 0, nil];
                    }
                }];
            }
            }
            else {
                people = nil;
                self.totalUsers.text = [NSString stringWithFormat:@"%u", 0, nil];
            }
            
        }
    }];

    CircleOverlay *overlay = [[CircleOverlay alloc] initWithCoordinate:aLocation.coordinate radius:self.radius];
    [self.mapView addOverlay:overlay];
    
    float circle = (kilometers / 100.0f) + 0.15;
    MKCoordinateSpan span;
    
    span.latitudeDelta = circle;
    span.longitudeDelta = circle;
    
    MKCoordinateRegion region;
    region.span = span;
    region.center = aLocation.coordinate;
    
    GeoQueryAnnotation *annotation = [[GeoQueryAnnotation alloc]
                                      initWithCoordinate:aLocation.coordinate
                                      radius:self.radius];
    
    [self.mapView addAnnotation:annotation];
}

- (void)configureOverlay
{
    if (self.location) {
        [self.mapView removeAnnotations:self.mapView.annotations];
        [self.mapView removeOverlays:self.mapView.overlays];
        [self.mapView setShowsUserLocation:NO];
        [self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
        [self setAddress:self.location];
        
        CircleOverlay *overlay = [[CircleOverlay alloc] initWithCoordinate:self.location.coordinate radius:self.radius];
        [self.mapView addOverlay:overlay];
        
        GeoQueryAnnotation *annotation = [[GeoQueryAnnotation alloc]
                                          initWithCoordinate:self.location.coordinate
                                          radius:self.radius];
        [self.mapView addAnnotation:annotation];
    }
}


#pragma mark - CLLocationManagerDelegate

/**
 Conditionally enable the Search/Add buttons:
 If the location manager is generating updates, then enable the buttons;
 If the location manager is failing, then disable the buttons.
 */

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    if (newLocation != nil) {
        self.location = newLocation;
    }
    
    self.navigationItem.leftBarButtonItem.enabled = YES;
    self.navigationItem.rightBarButtonItem.enabled = YES;
    
    CircleOverlay *overlay = [[CircleOverlay alloc] initWithCoordinate:self.location.coordinate radius:self.radius];
    [self.mapView addOverlay:overlay];
    
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    self.navigationItem.leftBarButtonItem.enabled = NO;
    self.navigationItem.rightBarButtonItem.enabled = NO;
}


#pragma mark - LocationViewController

/**
 Return a location manager -- create one if necessary.
 */
- (IBAction)locationBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES]; // go to previous vc
}

- (CLLocationManager *)locationManager {
	
    if (_locationManager != nil) {
		return _locationManager;
	}
	
	_locationManager = [[CLLocationManager alloc] init];
    _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    _locationManager.delegate = self;
	
	return _locationManager;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    return YES;
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField{

    if (textField == distanceField) {
        [distanceField resignFirstResponder];
    }

    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"InviteScreen"]) {
        InviteViewController *InviteViewController =
        segue.destinationViewController;
        InviteViewController.latitude = self.location.coordinate.latitude;
        InviteViewController.longitude = self.location.coordinate.longitude;
        InviteViewController.distance = distanceText;
        InviteViewController.country = self.country;
        InviteViewController.city = self.city;
        InviteViewController.address = self.searchDisplayController.searchBar.text;
        InviteViewController.meetingSpot = self.meetingSpotField.text;
        InviteViewController.people = people;

    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier
                                  sender:(id)sender {
    
    if ([identifier isEqualToString:@"InviteScreen"]) {
        
        if (    [self.searchDisplayController.searchBar.text length] == 0 ||
            [self.meetingSpotField.text length] == 0 ||
            [distanceField.text length] == 0 ||
            self.location == nil
            ) {
            
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Please fill in all the missing fields!"
                                  message:@""
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
            return NO;
        }
        else {
            return YES;
        }
    }
    
    return YES;
}

-(void)nextController:(id)sender
{
    if ( [self.searchDisplayController.searchBar.text length] == 0 ||
        [self.meetingSpotField.text length] == 0 ||
        [distanceField.text length] == 0 ||
        self.location == nil
        ) {
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Please fill in all the missing fields!"
                              message:@""
                              delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        InviteViewController *vc = (InviteViewController * ) [storyboard instantiateViewControllerWithIdentifier:@"InviteViewController"];

        vc.latitude = self.location.coordinate.latitude;
        vc.longitude = self.location.coordinate.longitude;
        vc.distance = distanceText;
        vc.country = self.country;
        vc.city = self.city;
        vc.address = self.searchDisplayController.searchBar.text;
        vc.meetingSpot = self.meetingSpotField.text;
        vc.people = people;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return !([newString length] > 2000);
}

@end