#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Parse/Parse.h>

@interface InviteViewController : UIViewController
<
    UITextFieldDelegate,
    UITextViewDelegate,
    UIPickerViewDataSource,
    UIPickerViewDelegate,
    CLLocationManagerDelegate
>
{
    UIDatePicker *datePicker;
    UIToolbar *datePikrToolbar;
    IBOutlet UITextField *reasonField;
    IBOutlet UITextField *durationField;
    NSArray *reasonChoices;
    NSArray *durationChoices;
    IBOutlet UITextField *dateField;
}

@property (strong, nonatomic) IBOutlet UITextField *dateField;
@property(nonatomic,retain) UIDatePicker *datePicker;
@property(nonatomic,retain) UIToolbar *datePikrToolbar;
@property(nonatomic,retain) NSDateFormatter *dateFormatter;
-(void)cancelDatePkrBtnActn;
-(void)doneDatePkrBtnActn;
@property (strong, nonatomic) IBOutlet UITextField *descriptionField;
@property (strong, nonatomic) PFObject *invitation;
@property (weak, nonatomic) IBOutlet UITextView *topicText;
@property (weak, nonatomic) NSString *distance;
@property (weak, nonatomic) NSArray *people;
@property (nonatomic) BOOL inviteError;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *meetingSpot;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (weak, nonatomic) IBOutlet UILabel *tips;
@property (weak, nonatomic) IBOutlet UILabel *nb;
- (IBAction)inviteButton:(id)sender;
- (IBAction)inviteBackButton:(id)sender;
@end
