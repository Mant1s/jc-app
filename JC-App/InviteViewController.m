#import "InviteViewController.h"
#import "ViewController.h"
#import <Parse/Parse.h>
#import "ViewInviteViewController.h"

@interface InviteViewController ()

@end

@implementation InviteViewController
UIDatePicker *datePicker;
NSDate *selectedDate;
UIPickerView *reasonPicker;
NSString *reasonText;
UIPickerView *durationPicker;
NSString *durationText;
NSString *meetingText;
bool isPickerDisplay;
@synthesize people;
@synthesize invitation;
@synthesize dateField,datePicker = _datePicker,datePikrToolbar = _datePikrToolbar,dateFormatter=_dateFormatter;
NSUInteger totalPeople;
NSUInteger peopleDone;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem * inviteButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Invite" style:UIBarButtonItemStylePlain target:self action:@selector(inviteButton:)];
    
    UIFont *font = [UIFont boldSystemFontOfSize:18];
    [inviteButtonItem setTitleTextAttributes:@{NSFontAttributeName: font}
                                  forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = inviteButtonItem;
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor redColor];
    
    self.dateField.delegate = self;
    self.descriptionField.delegate = self;
    self.topicText.delegate = self;
    meetingText = self.meetingSpot;
    self.tips.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
    self.nb.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
    
    [self.topicText.layer setBorderWidth:1.00];
    [self.topicText.layer setBorderColor:[[[UIColor lightGrayColor]
                                            colorWithAlphaComponent:0.5] CGColor]];
    
    reasonChoices = [[NSArray alloc]
                     initWithObjects:
                     @"Bible Study",
                     @"Devotion",
                     @"Fellowship",
                     @"Give Testimony",
                     @"Intervention",
                     @"Praise and Worship",
                     @"Prayer Request",
                     @"Teaching",
                     nil];
    
    durationChoices = [[NSArray alloc]
                       initWithObjects:
                       @"15 Min",
                       @"30 Min",
                       @"45 Min",
                       @"1 Hour",
                       nil];
    
    UIButton *cancelToolbarBtnCstm = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    cancelToolbarBtnCstm.frame=CGRectMake(0, 0, 80, 40);
    
    [cancelToolbarBtnCstm setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelToolbarBtnCstm addTarget:self action:@selector(cancelDatePkrBtnActn) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelToolbarBtn=[[UIBarButtonItem alloc] initWithCustomView:cancelToolbarBtnCstm];
    
    UIBarButtonItem *fixedSpaceToolbarITem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [fixedSpaceToolbarITem setWidth:140];
    
    UIButton *doneToolbarBtnCstm = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    doneToolbarBtnCstm.frame=CGRectMake(0, 0, 80, 40);
    [doneToolbarBtnCstm setTitle:@"Done" forState:UIControlStateNormal];
    [doneToolbarBtnCstm addTarget:self action:@selector(doneDatePkrBtnActn) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *doneToolbarBtn=[[UIBarButtonItem alloc] initWithCustomView:doneToolbarBtnCstm];
    
    NSMutableArray *toolbarinptArray=[[NSMutableArray alloc]init];
    [toolbarinptArray addObject:cancelToolbarBtn];
    [toolbarinptArray addObject:fixedSpaceToolbarITem];
    [toolbarinptArray addObject:doneToolbarBtn];
    
    _datePikrToolbar =[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,320,40)];
    [_datePikrToolbar setItems:toolbarinptArray animated:NO];
    
    _datePicker=[[UIDatePicker alloc] initWithFrame:CGRectMake(0,40,320,220)];
    
    _datePicker.datePickerMode=UIDatePickerModeDateAndTime;

    UIView *datePickWithToolbarView = [[UIView alloc] initWithFrame:CGRectMake(0,220,320,260)];
    [datePickWithToolbarView addSubview:_datePikrToolbar];
    [datePickWithToolbarView addSubview:_datePicker];
    
    [self.dateField setInputView:datePickWithToolbarView];
    
    reasonPicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
    reasonPicker.delegate = self;
    reasonPicker.dataSource = self;
    [reasonPicker setShowsSelectionIndicator:YES];
    reasonField.inputView = reasonPicker;
    
    durationPicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
    durationPicker.delegate = self;
    durationPicker.dataSource = self;
    [durationPicker setShowsSelectionIndicator:YES];
    durationField.inputView = durationPicker;

    /*
     * Reason Picker
     */
    
    UIToolbar*  reasonToolbar = [[UIToolbar alloc]
                                 initWithFrame:CGRectMake(0, 0, 320, 40)];
    [reasonToolbar sizeToFit];
    
    NSMutableArray *reasonBarItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *reasonFlexSpace = [[UIBarButtonItem alloc]
                    initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                    target:self
                    action:nil];
    
    [reasonBarItems addObject:reasonFlexSpace];
    UIBarButtonItem *reasonDoneBtn = [[UIBarButtonItem alloc]
                            initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                            target:self
                            action:@selector(reasonDoneClicked)];
    
    [reasonBarItems addObject:reasonDoneBtn];
    [reasonToolbar setItems:reasonBarItems animated:YES];
    
    reasonField.inputAccessoryView = reasonToolbar;
    
    /*
     * Duration Picker
     */
    
    UIToolbar*  durationToolbar = [[UIToolbar alloc]
                                   initWithFrame:CGRectMake(0, 0, 320, 40)];
    [reasonToolbar sizeToFit];

    NSMutableArray *durationBarItems = [[NSMutableArray alloc] init];
    UIBarButtonItem *durationFlexSpace = [[UIBarButtonItem alloc]
                    initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                    target:self
                    action:nil];
    
    [durationBarItems addObject:durationFlexSpace];
    UIBarButtonItem *durationDoneBtn = [[UIBarButtonItem alloc]
                            initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                            target:self
                            action:@selector(durationDoneClicked)];
    
    [durationBarItems addObject:durationDoneBtn];
    [durationToolbar setItems:durationBarItems animated:YES];
    
    durationField.inputAccessoryView = durationToolbar;
}

-(void)cancelDatePkrBtnActn{
    [self.dateField resignFirstResponder];
}

-(void)doneDatePkrBtnActn{
    if (_datePicker.date == [NSDate date])
    {
        NSDateFormatter *dateFormatter;
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        [dateFormatter setDateFormat:@"EEEE dd MMM YYYY HH:mm"];
        self.dateField.text = [NSString stringWithFormat:@"%@",
                               [dateFormatter stringFromDate:selectedDate]];
    }
    else
    {
        NSDateFormatter *dateFormatter;
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        [dateFormatter setDateFormat:@"EEEE dd MMM YYYY HH:mm"];
        self.dateField.text = [NSString stringWithFormat:@"%@",
                               [dateFormatter stringFromDate:_datePicker.date]];
        
    }
    
    [self.dateField resignFirstResponder];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

-(void)reasonDoneClicked
{
    if (reasonText == nil)
    {
        reasonField.text = reasonChoices[0];
    }
    else {
        reasonField.text = reasonText;
    }
    
    [reasonField resignFirstResponder];
}

-(void)durationDoneClicked
{
    if (durationText == nil)
    {
        durationField.text = durationChoices[0];
    }
    else {
        durationField.text = durationText;
    }
    
    [durationField resignFirstResponder];
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView
            numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == reasonPicker) {
        return reasonChoices.count;
    }
    else if (pickerView == durationPicker) {
        return durationChoices.count;
    }
    return 0;
}
- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView == durationPicker) {
        return [durationChoices objectAtIndex:row];
    }
    
    return [reasonChoices objectAtIndex:row];
}
- (void) pickerView:(UIPickerView *)pickerView
        didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == reasonPicker) {
        reasonText = (NSString *)[reasonChoices objectAtIndex:row];
    }
    else if (pickerView == durationPicker) {
        durationText = (NSString *)[durationChoices objectAtIndex:row];
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
                                       replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
        return YES;
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField{
    
    if (textField == self.dateField) {
        [self.descriptionField becomeFirstResponder];
        [self.descriptionField resignFirstResponder];
    }
    
    if (textField == self.descriptionField) {
        [textField resignFirstResponder];
    }
    
    return YES;
}

- (IBAction)inviteButton:(id)sender {
    
    if (    [reasonField.text length] == 0 ||
            _datePicker.date == nil ||
            [durationField.text length] == 0 ||
            [self.topicText.text length] == 0
        )
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Please fill in all the missing fields!"
                              message:@""
                              delegate:self
                              cancelButtonTitle: nil
                              otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else {
        @try
        {
            PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLatitude:
                                    self.latitude
                                    longitude:self.longitude];
            
            PFQuery *userQuery = [PFUser query];
            
            [userQuery getObjectInBackgroundWithId:[PFUser currentUser].objectId block:^(PFObject *userObject, NSError *error) {
                if (userObject[@"geolocation"] == nil )
                {
                    [userObject setObject:geoPoint forKey:@"geolocation"];
                    [userObject saveInBackground];
                }
            }];
            
            PFObject *locationObject = [PFObject objectWithClassName:@"location"];
            [locationObject setObject:geoPoint forKey:@"geolocation"];
            locationObject[@"country"] = self.country;
            locationObject[@"city"] = self.city;
            locationObject[@"address"] = self.address;
            locationObject[@"meetingPoint"] = meetingText;
        
            PFObject *invite = [PFObject objectWithClassName:@"invite"];
        
            [invite setObject:[PFUser currentUser] forKey:@"user"];
            [invite setObject:geoPoint forKey:@"location"];
            [invite setObject:_datePicker.date forKey:@"date"];

            invite[@"reason"] = reasonField.text;
            invite[@"duration"] = durationField.text;
            invite[@"topic"] = self.topicText.text;
            invite[@"location"] = locationObject;
            
            
            PFObject *attendance = [PFObject objectWithClassName:@"invite_attendance"];
            
            [attendance setObject:[PFUser currentUser] forKey:@"attendee"];
            [attendance setObject:[NSNumber numberWithBool:YES] forKey:@"attending"];
            [attendance setObject:_datePicker.date forKey:@"expire"];
            
            attendance[@"invite"] = invite;
            
            
            [attendance saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                
                if (error) {
                    self.inviteError = YES;
                    return;
                }
                
                if (succeeded) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.inviteError = NO;
                    });
                    
                    
                }
                else {
                    self.inviteError = YES;
                }
            }];
            
            if (people != nil) {
            
            totalPeople = people.count;
                
            for (PFObject *object in people)
            {
                PFObject *attendance = [PFObject objectWithClassName:@"invite_attendance"];
                
                [attendance setObject:object forKey:@"attendee"];
                [attendance setObject:[NSNumber numberWithBool:NO] forKey:@"attending"];
                [attendance setObject:_datePicker.date forKey:@"expire"];
            
                attendance[@"invite"] = invite;
            
                [attendance saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
                    if (error) {
                        self.inviteError = YES;
                        return;
                    }
                    
                    if (succeeded) {
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            self.inviteError = NO;
                            [self sendPushNotification:object];
                            peopleDone += 1;
                        });
                 
                    }
                    else {
                        self.inviteError = YES;
                    }
                }];
                
            }
                
            }
            
            if (self.inviteError) {
                UIAlertView *alertView = [[UIAlertView alloc]
                                          initWithTitle:@"Error occured!"
                                          message:nil
                                          delegate:self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"OK", nil];
                [alertView show];
            }
            else {
                    invitation = invite;
                    UIAlertView *alert = [[UIAlertView alloc]
                                          initWithTitle:@"Successfully Sent Invite!"
                                          message:@""
                                          delegate:self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"OK", nil];
                    [alert setTag:2];
                    [alert show];
            }
        }
    
        @catch (NSException *ex) {
      
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message:[NSString stringWithFormat:@"%@",ex]
                                  delegate:self
                                  cancelButtonTitle:nil
                                  otherButtonTitles: @"OK", nil];
            [alert show];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if ([alertView tag] == 2) {
        if (buttonIndex == 0) {
            NSLog(@"Total People : %lu", (unsigned long)totalPeople);
            NSLog(@"People Done: %lu", (unsigned long)peopleDone);
            if (invitation != nil && totalPeople == peopleDone) {
                
                UIStoryboard *storyboard = self.storyboard;
                
                ViewInviteViewController *vc = [storyboard
                                                instantiateViewControllerWithIdentifier:@"ViewInvite"];
                [vc setInvite:invitation];
                
                PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLatitude:
                                        self.latitude
                                                              longitude:self.longitude];
                
                PFQuery *userQuery = [PFUser query];
                
                [userQuery getObjectInBackgroundWithId:[PFUser currentUser].objectId block:^(PFObject *userObject, NSError *error) {
                    if (userObject[@"geolocation"] == nil )
                    {
                        [userObject setObject:geoPoint forKey:@"geolocation"];
                        [userObject saveInBackground];
                        if(!self.inviteError) {
                            [self.navigationController pushViewController:vc animated:YES];
                        }
                    }
                    else
                    {
                        if(!self.inviteError) {
                            [self.navigationController pushViewController:vc animated:YES];
                        }
                    }
                }];
                

            } else {
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle:nil
                                      message:[NSString stringWithFormat:@"Please wait while your invitation is being sent..."]
                                      delegate:self
                                      cancelButtonTitle:nil
                                      otherButtonTitles: @"OK", nil];
                [alert setTag:2];
                [alert show];
            }
        }
    }
}


- (void) sendPushNotification:(PFObject *)person
{
    PFQuery *pushQuery = [PFInstallation query];
    [pushQuery whereKey:@"user" equalTo:person];
    
    NSString *message = [NSString stringWithFormat:
                         @"%@ %@ %@ %@ %@ %@",
                         @" You are invited to \n",
                         @"a",
                         reasonField.text,
                         @"\n for",
                         self.topicText.text,
                         @"\n Can you make it?"
                         ];
    
    NSDictionary *data = @{
                           @"alert": message,
                           @"i": invitation.objectId
                           };
    PFPush *push = [[PFPush alloc] init];
    [push expireAtDate:[invitation objectForKey:@"date"]];
    [push setQuery:pushQuery];
    [push setData:data];
    [push sendPushInBackground];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)inviteBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return !([newString length] > 2000);
}
@end