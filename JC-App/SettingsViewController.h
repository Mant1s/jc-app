//
//  SettingsViewController.h
//  JC-App
//
//  Created by Frans Lourens on 2014/04/09.
//  Copyright (c) 2014 automytest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CircleOverlay.h"

//the size of the visible area around center
const double latitudeDelta = 0.00;
const double longitudeDelta = 0.00;
const double radius = 50000.00;

@class SPGooglePlacesAutocompleteQuery;

@interface SettingsViewController : UIViewController
<
UITextFieldDelegate,
UITextViewDelegate,
UITableViewDelegate,
UITableViewDataSource,
UISearchDisplayDelegate,
UISearchBarDelegate,
MKMapViewDelegate,
CLLocationManagerDelegate,
UIPickerViewDataSource,
UIPickerViewDelegate
>
{
    NSArray *searchResultPlaces;
    MKPointAnnotation *selectedPlaceAnnotation;
    SPGooglePlacesAutocompleteQuery *searchQuery;
    BOOL shouldBeginEditing;
    NSArray *gpsChoices;
    IBOutlet UITextField *gps;
    UIDatePicker *beginDatePicker;
    UIToolbar *beginDatePikrToolbar;
    UIDatePicker *endDatePicker;
    UIToolbar *endDatePikrToolbar;
}

@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
//@property (nonatomic) UISearchDisplayController *searchController;

@property (nonatomic, retain) CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, assign) CLLocationDistance radius;
@property (nonatomic, strong) CircleOverlay *targetOverlay;

@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *city;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

- (IBAction)saveSettings:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *monday;
@property (weak, nonatomic) IBOutlet UISwitch *tuesday;
@property (weak, nonatomic) IBOutlet UISwitch *wednesday;
@property (weak, nonatomic) IBOutlet UISwitch *thursday;
@property (weak, nonatomic) IBOutlet UISwitch *friday;
@property (weak, nonatomic) IBOutlet UISwitch *saturday;
@property (weak, nonatomic) IBOutlet UISwitch *sunday;
@property (weak, nonatomic) IBOutlet UITextField *beginTime;
@property (weak, nonatomic) IBOutlet UITextField *endTime;
@property(nonatomic,retain) UIDatePicker *beginDatePicker;
@property(nonatomic,retain) UIToolbar *beginDatePikrToolbar;
@property(nonatomic,retain) NSDateFormatter *beginDateFormatter;
@property(nonatomic,retain) UIDatePicker *endDatePicker;
@property(nonatomic,retain) UIToolbar *endDatePikrToolbar;
@property(nonatomic,retain) NSDateFormatter *endDateFormatter;
-(void)cancelBeginDatePkrBtnActn;
-(void)doneBeginDatePkrBtnActn;
-(void)cancelEndDatePkrBtnActn;
-(void)doneEndDatePkrBtnActn;
@end
