//
//  main.m
//  JC-App
//
//  Created by Frans Lourens on 2014/03/13.
//  Copyright (c) 2014 automytest. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
