#import "ViewController.h"
#import <Parse/Parse.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    //[self.homeLabel setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"jcapp.jpg"]]];
    self.navigationItem.hidesBackButton = YES;
    
    
    
    
    self.inviteOther.backgroundColor = [[UIColor alloc] initWithRed:0.93 green:0.93
                                                                blue:0.93 alpha:1.0];
    
    self.inviteOther.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
    
    self.inviteNotAccept.backgroundColor = [[UIColor alloc] initWithRed:0.93 green:0.93
                                                      blue:0.93 alpha:1.0];
    
    self.settingsLabel.backgroundColor = [[UIColor alloc] initWithRed:0.93 green:0.93
                                                                blue:0.93 alpha:1.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)whatButton:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Find fellow Christians close to you. \n You’ll never be alone again, just spread the Word!"
                          message:@""
                          delegate:self
                          cancelButtonTitle:nil
                          otherButtonTitles:@"OK", nil];
    [alert show];
}
@end
