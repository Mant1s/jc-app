//
//  NewInvitesViewController.h
//  JC-App
//
//  Created by Frans Lourens on 2014/03/31.
//  Copyright (c) 2014 automytest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "AcceptedInvitesViewController.h"

@interface NewInvitesViewController : PFQueryTableViewController <
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong) AcceptedInvitesViewController *viewController;
@property (retain, nonatomic) UINavigationController *navController;

@end