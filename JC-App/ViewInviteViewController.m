//
//  ViewInviteViewController.m
//  JC-App
//
//  Created by Frans Lourens on 2014/05/05.
//  Copyright (c) 2014 automytest. All rights reserved.
//

#import "ViewInviteViewController.h"
#import "GeoQueryAnnotation.h"

@interface ViewInviteViewController ()

@end

@implementation ViewInviteViewController
@synthesize mapView;
BOOL attending;
NSString *message;
UIAlertView *alertView;

enum PinAnnotationTypeTag {
    PinAnnotationTypeTagGeoQuery = 1
};

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Home" style:UIBarButtonItemStylePlain target:self action:@selector(popToRootViewController:)];
    
    PFObject *location = [self.invite objectForKey:@"location"];
    PFGeoPoint *geo = [location objectForKey:@"geolocation"];
    
    CLLocationCoordinate2D geolocation;
    geolocation.latitude = geo.latitude;
    geolocation.longitude = geo.longitude;
    
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.0;
    span.longitudeDelta = 0.0;
    region.span = span;
    region.center = geolocation;
    
    self.mapView.userInteractionEnabled = NO;
    [self.mapView setRegion:region animated:TRUE];
    [self.mapView regionThatFits:region];
    
    GeoQueryAnnotation *annotation = [[GeoQueryAnnotation alloc]
                                      initWithCoordinate:geolocation
                                      radius: 0];
    [self.mapView addAnnotation:annotation];
    
    self.acceptButton.enabled = NO;
    NSDate *now = [self.invite objectForKey:@"date"];
    
    
    
    self.iUser = [self.invite objectForKey:@"user"];

    if ([[PFUser currentUser].objectId isEqualToString:self.iUser.objectId]) {
        if([[NSDate date] compare:now] == NSOrderedAscending) {
        self.acceptButton.layer.borderWidth = 1.0f;
        [self.acceptButton setTitle:@"Cancel this invite" forState:UIControlStateNormal];
        self.acceptButton.enabled = YES;
        }
        //self.acceptAction.selectedSegmentIndex = 1;
        //[self.acceptAction setEnabled:NO forSegmentAtIndex:1];
        //[self.acceptAction setTitle:@"Cancel" forSegmentAtIndex:0];
        //[self.acceptAction removeSegmentAtIndex:1 animated:NO];
    } else {
        //self.acceptLabel.text = nil;
        //[self.acceptAction removeSegmentAtIndex:1 animated:NO];
        
        PFQuery *acceptQuery = [PFQuery queryWithClassName:@"invite_attendance"];
        [acceptQuery whereKey:@"attendee" equalTo:[PFUser currentUser]];
        [acceptQuery whereKey:@"invite" equalTo:self.invite];
        
        [acceptQuery getFirstObjectInBackgroundWithBlock:^(PFObject *acceptedInvite, NSError *error) {
            attending = [acceptedInvite[@"attending"] boolValue];
           
            //has three possible values: NSOrderedSame,NSOrderedDescending, NSOrderedAscending
            
            
            if(attending) {
                if([[NSDate date] compare:now] == NSOrderedAscending) {
                    //[self.acceptAction setTitle:@"Decline" forSegmentAtIndex:0];
                    self.acceptButton.layer.borderWidth = 1.0f;
                    [self.acceptButton setTitle:@"Decline invite" forState:UIControlStateNormal];
                    self.acceptButton.enabled = YES;
                }
            } else {
                if([[NSDate date] compare:now] == NSOrderedAscending) {
                    //[self.acceptAction setTitle:@"Accept" forSegmentAtIndex:0];
                    self.acceptButton.layer.borderWidth = 1.0f;
                    [self.acceptButton setTitle:@"Accept invite" forState:UIControlStateNormal];
                    self.acceptButton.enabled = YES;
                }
            }
        }];
    }
    
    PFQuery *query = [PFQuery queryWithClassName:@"invite_attendance"];
    [query whereKey:@"invite" equalTo:self.invite];
    [query includeKey:@"attendee"];
    [query whereKey:@"attendee" notEqualTo:[PFUser currentUser]];
    [query countObjectsInBackgroundWithBlock:^(int count, NSError *error) {
        if (!error) {
            totalCount = [NSNumber numberWithInt:count];
            //NSLog(@"%@", totalCount);
            self.total.text = [NSString stringWithFormat:@"%@",totalCount];
        } else {
            self.total.text = [NSString stringWithFormat:@"%d",0];
        }
        
    }];
    
    PFQuery *query2 = [PFQuery queryWithClassName:@"invite_attendance"];
    [query2 whereKey:@"invite" equalTo:self.invite];
    [query2 whereKey:@"attending" equalTo:[NSNumber numberWithBool:YES]];
    [query2 includeKey:@"attendee"];
    [query2 whereKey:@"attendee" notEqualTo:[PFUser currentUser]];
    [query2 countObjectsInBackgroundWithBlock:^(int count2, NSError *error) {
        if (!error) {
            NSLog(@"Count %d", count2 );
            if (count2 > 1) {
            totalAcceptedCounts = [NSNumber numberWithInt:count2 - 1];
            } else {
                totalAcceptedCounts = [NSNumber numberWithInt:count2];
            }
            //NSLog(@"%@", totalAcceptedCounts);
            self.totalAcceptedCount.text = [NSString stringWithFormat:@"%@",totalAcceptedCounts];
        } else {
            self.totalAcceptedCount.text = [NSString stringWithFormat:@"%d",0];
        }
        
    }];

    self.addressLabel.text = [NSString stringWithFormat:@"Near: %@",[location objectForKey:@"address"]];
    self.meetingSpotLabel.text = [location objectForKey:@"meetingPoint"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSString *datetime = [formatter stringFromDate:now];
    
    self.reasonLabel.text = [NSString stringWithFormat:@"Reason: %@",[self.invite objectForKey:@"reason"]];
    self.topicLabel.text = [NSString stringWithFormat:@"Topic: %@",[self.invite objectForKey:@"topic"]];
    self.durationLabel.text = [NSString stringWithFormat:@"Duration: %@",[self.invite objectForKey:@"duration"]];
    self.dateLabel.text = [NSString stringWithFormat:@"Date: %@",datetime];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

#pragma mark - MKMapViewDelegate
- (MKAnnotationView *)mapView:(MKMapView *)mapViewIn
            viewForAnnotation:(id <MKAnnotation>)annotation
{
    if (    mapViewIn != self.mapView ||
        [annotation isKindOfClass:[MKUserLocation class]]
        )
    {
        return nil;
    }
    
    static NSString *GeoQueryAnnotationIdentifier = @"SPGooglePlacesAutocompleteAnnotation";
    
    if ([annotation isKindOfClass:[GeoQueryAnnotation class]]) {
        MKPinAnnotationView *annotationView =
        (MKPinAnnotationView *)[mapView
                                dequeueReusableAnnotationViewWithIdentifier:GeoQueryAnnotationIdentifier];
        
        if (!annotationView) {
            annotationView = [[MKPinAnnotationView alloc]
                              initWithAnnotation:annotation
                              reuseIdentifier:GeoQueryAnnotationIdentifier];
            annotationView.tag = PinAnnotationTypeTagGeoQuery;
            annotationView.canShowCallout = NO;
            annotationView.pinColor = MKPinAnnotationColorRed;
            annotationView.animatesDrop = NO;
            annotationView.draggable = YES;
        }
        
        return annotationView;
    }
    
    return nil;
}

- (IBAction)confirmAction:(id)sender {
    NSLog(@"Pressed Button!");
    
    NSLog(@"The button title is %@",self.acceptButton.titleLabel.text);
    
    if([self.acceptButton.titleLabel.text  isEqual: @"Cancel this invite"])
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Are you sure you want to cancel this invitation?"
                              message:@""
                              delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:@"OK", nil];
        [alert setTag:1];
        [alert show];
    }
    else if ([self.acceptButton.titleLabel.text  isEqual: @"Accept invite"])
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Are you sure you want to accept this invitation?"
                              message:@""
                              delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:@"OK", nil];
        [alert setTag:2];
        [alert show];
    }
    else if ([self.acceptButton.titleLabel.text  isEqual: @"Decline invite"])
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Are you sure you want to decline this invitation?"
                              message:@""
                              delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:@"OK", nil];
        [alert setTag:3];
        [alert show];
    }
}

- (NSString *) setPushMessage:(PFObject *)aLocation {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSString *onDate = [dateFormatter stringFromDate:[self.invite objectForKey:@"date"]];
    
    [dateFormatter setDateFormat:@"hh-mm"];
    NSString *onTime = [dateFormatter stringFromDate: [self.invite objectForKey:@"date"]];
    
    NSString *message = [NSString stringWithFormat:
                         @"%@ %@ %@ %@ %@ %@ %@ %@ %@ %@ %@ %@ %@ %@ %@",
                         @" You are invited to \n",
                         @"a",
                         [self.invite objectForKey:@"reason"],
                         @"\n for",
                         [self.invite objectForKey:@"topic"],
                         @"\n at",
                         [aLocation objectForKey:@"address"],
                         [aLocation objectForKey:@"city"],
                         @"\n at",
                         onDate,
                         @"on",
                         onTime,
                         @"\n for",
                         [self.invite objectForKey:@"duration"],
                         @"\n Can you make it?"
                         ];
    
    return message;
}

- (void) getPushNotification:(PFObject *)aInvite
{
    @try
    {
        PFQuery *query = [PFQuery queryWithClassName:@"invite_attendance"];
        [query whereKey:@"attendee" equalTo:[PFUser currentUser]];
        [query whereKey:@"invite" equalTo:aInvite];
        
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *acceptedInvite, NSError *error) {
            if (!error) {
                if (acceptedInvite != nil) {
                    
                    attending = [acceptedInvite[@"attending"] boolValue];
                    
                    if(attending) {
                        //self.invite = nil;
                        alertView = [[UIAlertView alloc]
                                     initWithTitle:[[error userInfo]
                                                    objectForKey:@"error"]
                                     message:@"You allready accepted this invite"
                                     delegate:self
                                     cancelButtonTitle:@"Cancel"
                                     otherButtonTitles:nil, nil];
                        [alertView show];
                    } else {
                        
                        self.invite = aInvite;
                        NSString *locationId = [[aInvite objectForKey:@"location"] objectId];
                        PFQuery *query = [PFQuery queryWithClassName:@"location"];
                        
                        [query getObjectInBackgroundWithId:locationId block:^(PFObject *location, NSError *error) {
                            if (!error) {
                                alertView = [[UIAlertView alloc]
                                             initWithTitle: @"JC-App"
                                             message: [self setPushMessage:location]
                                             delegate: self
                                             cancelButtonTitle: nil
                                             otherButtonTitles: @"Yes", @"Ignore", nil];
                                [alertView show];
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                });
                                
                            }
                        }];
                    }
                }
                else {
                    NSLog(@"Error : %@",error.description);
                }
            }
        }];
    }
    @catch (NSException *ex) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Error"
                              message:[NSString stringWithFormat:@"%@",ex]
                              delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles: nil];
        [alert show];
    }
}

- (void) acceptPushNotification:(BOOL)aResponse {
    @try
    {
        if (self.invite != nil) {
            
            PFQuery *query = [PFQuery queryWithClassName:@"invite_attendance"];
            [query whereKey:@"attendee" equalTo:[PFUser currentUser]];
            [query whereKey:@"invite" equalTo:self.invite];
            
            [query getFirstObjectInBackgroundWithBlock:^(PFObject *acceptedInviteObject, NSError *error) {
                if (!acceptedInviteObject) {
                    NSLog(@"The getFirstObject request failed.");
                } else {
                    
                    if(aResponse) {
                        [acceptedInviteObject setObject:[NSNumber numberWithBool:YES] forKey:@"attending"];
                    }
                    else {
                        [acceptedInviteObject setObject:[NSNumber numberWithBool:NO] forKey:@"attending"];
                    }
                    
                    [acceptedInviteObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        if (!error) {
                            if (succeeded) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    if (aResponse) {
                                        UIViewController *myController = [self.storyboard instantiateViewControllerWithIdentifier:@"AcceptedInvitesView"];
                                        [self.navigationController pushViewController:myController animated:NO];
                                    }
                                });
                            }
                            else {
                                NSLog(@"Failed to save.");
                            }
                        }
                        else {
                            alertView = [[UIAlertView alloc]
                                         initWithTitle:[[error userInfo]
                                                        objectForKey:@"error"]
                                         message:error.description
                                         delegate:self
                                         cancelButtonTitle:nil
                                         otherButtonTitles:@"Ok", nil];
                            return;
                        }
                    }];
                }
            }];
        }
    }
    @catch (NSException *ex) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Error"
                              message:[NSString stringWithFormat:@"%@",ex]
                              delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles: nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView tag] == 1)  //cancel
    {
        if (buttonIndex == 1) {
            NSLog(@"Pressed OK at Cancel");
        PFQuery *query = [PFQuery queryWithClassName:@"invite_attendance"];
        [query whereKey:@"invite" equalTo:self.invite];
        //[query whereKey:@"attending" equalTo:[NSNumber numberWithBool:YES]];
        [query includeKey:@"attendee"];
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                for (PFObject *object in objects) {
                    PFObject *aUser = [object objectForKey:@"attendee"];
            
                    if ([aUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
                        PFObject *userInvite = [object objectForKey:@"invite"];
                        [userInvite deleteInBackground];
                        [object deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                            if (succeeded) {
                                NSLog(@"Success");
                            } else {
                                NSLog(@"%@", error);
                            }
                        }];
                        
                    } else
                    {
                        BOOL attending = [object[@"attending"] boolValue];
                        
                        if(attending) {
                            [self sendPushNotification:aUser];
                        }
                        
                        [object deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                            if (succeeded) {
                                NSLog(@"Success");
                            } else {
                                NSLog(@"%@", error);
                            }
                        }];
                    }
                }
                UIViewController *myController = [self.storyboard instantiateViewControllerWithIdentifier:@"SentInvites"];
                [self.navigationController pushViewController:myController animated:NO];
            } else {
                // Log details of the failure
                NSLog(@"Error: %@ %@", error, [error userInfo]);
            }
        }];
        } else {
            NSLog(@"Pressed Cancel at Cancel");
        }
    }
    else if ([alertView tag] == 2) //accept
    {
        if (buttonIndex == 1) {
            NSLog(@"Pressed OK at Accept");
            [self acceptPushNotification:YES];
        }
    }
    else if ([alertView tag] == 3) //decline
    {
        if (buttonIndex == 1) {
            NSLog(@"Pressed OK at decline");
            [self acceptPushNotification:NO];
        }
    }
    else
    {
        if (buttonIndex == 1) {
            NSLog(@"Pressed OK");
            [self acceptPushNotification:YES];
        } else {
            [self acceptPushNotification:NO];
        }
    }
}

- (void) sendPushNotification:(PFObject *)person
{
    PFQuery *pushQuery = [PFInstallation query];
    [pushQuery whereKey:@"user" equalTo:person];
    
    NSLog(@"Invitation Push ID : %@", self.invite.objectId);
    
    NSString *message = [NSString stringWithFormat:
                         @"%@ %@ %@ %@",
                         @" Invitation cancelled for  : ",
                         [self.invite objectForKey:@"reason"],
                         @"\n",
                         [self.invite objectForKey:@"topic"]
                         ];
    PFPush *push = [[PFPush alloc] init];
    [push setQuery:pushQuery];
    [push setMessage:message];
    [push sendPushInBackground];
    
}

-(void)popToRootViewController:(id)sender
{
    UIViewController *myController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
    [self.navigationController pushViewController:myController animated:NO];
}
@end
