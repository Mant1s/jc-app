//
//  TermsAndConditionsViewController.m
//  JC-App
//
//  Created by Frans Lourens on 2014/04/23.
//  Copyright (c) 2014 automytest. All rights reserved.
//

#import "TermsAndConditionsViewController.h"
#import "ViewController.h"
#import <Parse/Parse.h>

@interface TermsAndConditionsViewController ()

@end

@implementation TermsAndConditionsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)unwindToExit:(UIStoryboardSegue *)segue
{
    //returning to A
}
@end
