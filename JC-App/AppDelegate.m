#import <Parse/Parse.h>
#import "AppDelegate.h"
#import "ViewInviteViewController.h"
#import "ViewController.h"
#import "LocationViewController.h"

@implementation AppDelegate
@synthesize window;
@synthesize navController;

- (BOOL)application:(UIApplication *)application
        didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Parse setApplicationId:@"WHEIauFcdjs2Yl4e0paqAoTNQjmUaHF0hsX4N5fT"
                  clientKey:@"XLqt8Gsc4o1usZuyAm7HFr6fycILR7fVxZcVx9g3"];
    
    PFACL *defaultACL = [PFACL ACL];
    [defaultACL setPublicReadAccess:YES];
    [defaultACL setPublicWriteAccess:YES];
    [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];
    
    [PFUser enableAutomaticUser];
    [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            PFQuery *weekQuery = [PFQuery queryWithClassName:@"notification"];
            [weekQuery whereKey:@"user" equalTo:[PFUser currentUser]];
            
            [weekQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                if (error.code == 101) {
                    PFObject *weekObject = [PFObject objectWithClassName:@"notification"];
                    
                    weekObject[@"Monday"] = [NSNumber numberWithBool:YES];
                    weekObject[@"Tuesday"] = [NSNumber numberWithBool:YES];
                    weekObject[@"Wednesday"] = [NSNumber numberWithBool:YES];
                    weekObject[@"Thursday"] = [NSNumber numberWithBool:YES];
                    weekObject[@"Friday"] = [NSNumber numberWithBool:YES];
                    weekObject[@"Saturday"] = [NSNumber numberWithBool:YES];
                    weekObject[@"Sunday"] = [NSNumber numberWithBool:YES];
                    weekObject[@"user"] = [PFUser currentUser];
                    [weekObject saveInBackground];
                }
            }];
        }
    }];
    
    PFInstallation *installation = [PFInstallation currentInstallation];
    installation[@"user"] = [PFUser currentUser];
    [installation saveInBackground];
    
    self.viewController = [[ViewController alloc] init];
    
    self.navController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    self.navController.navigationItem.hidesBackButton = YES;
    [application registerForRemoteNotificationTypes:
     UIRemoteNotificationTypeBadge |
     UIRemoteNotificationTypeAlert |
     UIRemoteNotificationTypeSound];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{

}

- (void)applicationDidEnterBackground:(UIApplication *)application
{

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{

}

- (void)applicationWillTerminate:(UIApplication *)application
{

}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken {
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:newDeviceToken];
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application
    didReceiveRemoteNotification:(NSDictionary *)userInfo
    fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))handler {
    
    self.navController.delegate = self;
    
    UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    ViewInviteViewController *controller = (ViewInviteViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"ViewInvite"];
    
    //ViewController *vc = (ViewController * ) [mainStoryboard instantiateViewControllerWithIdentifier:@"Home"];
    
    if ([userInfo objectForKey:@"i"] != nil) {
    
    NSString *inviteId = [userInfo objectForKey:@"i"];

    PFQuery *targetInvite = [PFQuery queryWithClassName:@"invite"];
    [targetInvite includeKey:@"location"];
    [targetInvite includeKey:@"user"];
    [targetInvite getObjectInBackgroundWithId:inviteId block:^(PFObject *object, NSError *error) {
        if (error) {
            handler(UIBackgroundFetchResultFailed);
        } else if ([PFUser currentUser]) {
            
            [controller setInvite:object];
            [navigationController pushViewController:controller animated:YES];
            
            //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            //ViewInviteViewController *vc = (ViewInviteViewController * ) [storyboard instantiateViewControllerWithIdentifier:@"ViewInvite"];
            //[vc setInvite:object];
            //[vc getPushNotification:object];
            //UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController: vc];
            //self.window.rootViewController = navVC;
            handler(UIBackgroundFetchResultNewData);
        } else {
            handler(UIBackgroundFetchResultNoData);
        }
    }];
        
    } else {
        //UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController: vc];
        //self.window.rootViewController = navVC;
        [PFPush handlePush:userInfo];
    }
}
@end
