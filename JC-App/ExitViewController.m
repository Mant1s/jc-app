//
//  ExitViewController.m
//  JC-App
//
//  Created by Frans Lourens on 2014/04/25.
//  Copyright (c) 2014 automytest. All rights reserved.
//

#import "ExitViewController.h"
#import "TermsAndConditionsViewController.h"

@interface ExitViewController ()

@end

@implementation ExitViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = NO;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
