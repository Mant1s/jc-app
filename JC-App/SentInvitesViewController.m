//
//  SentInvitesViewController.m
//  JC-App
//
//  Created by Frans Lourens on 2014/04/25.
//  Copyright (c) 2014 automytest. All rights reserved.
//

#import "SentInvitesViewController.h"
#import "ViewInviteViewController.h"
#import "UpdatesTableViewCell.h"

@interface SentInvitesViewController ()

@end

@implementation SentInvitesViewController

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        self.pullToRefreshEnabled = YES;
        self.paginationEnabled = YES;
        self.objectsPerPage = 5;
    }
    return self;
}

- (PFQuery *)queryForTable {
    PFQuery *query = [PFQuery queryWithClassName:@"invite"];
    [query whereKey:@"user" equalTo:[PFUser currentUser]];
    [query includeKey:@"location"];
    [query includeKey:@"user"];
    
    if ([self.objects count] == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    
    [query orderByDescending:@"date"];
    
    return query;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
                        object:(PFObject *)object {
    
    static NSString *simpleTableIdentifier = @"inviteViewCell";
    
    UpdatesTableViewCell *cell = (UpdatesTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"UpdatesListView" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    PFObject *location = [object objectForKey:@"location"];
    
    NSDate *now = [object objectForKey:@"date"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSString *datetime = [formatter stringFromDate:now];
    
    cell.descriptionLabel.backgroundColor = (indexPath.row%2)?[UIColor clearColor]:[UIColor lightGrayColor];
    cell.descriptionLabel.editable = NO;
    cell.titleLabel.text = [object objectForKey:@"reason"];
    cell.descriptionLabel.text = [object objectForKey:@"topic"];
    cell.locationLabel.text = [NSString stringWithFormat:@"Near: %@, %@",
                               [location objectForKey:@"address"],
                               [location objectForKey:@"city"]];
    //cell.locationLabel.enabled = NO;
    //cell.locationLabel.textColor = [UIColor blackColor];
    cell.exactMeetingPointLabel.text = [location objectForKey:@"meetingPoint"];
    cell.dateLabel.text = datetime;
    cell.timelabel.text = [object objectForKey:@"duration"];
    
    cell.backgroundColor = (indexPath.row%2)?[UIColor clearColor]:[UIColor lightGrayColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    PFObject *selectedObject = [self objectAtIndexPath:indexPath];
    
    UIStoryboard *storyboard = self.storyboard;
    
    ViewInviteViewController *vc = [storyboard
                                              instantiateViewControllerWithIdentifier:@"ViewInvite"];

    [vc setInvite:selectedObject];

    [self.navigationController pushViewController:vc animated:YES];
    
}

@end
