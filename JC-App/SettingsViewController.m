//
//  SettingsViewController.m
//  JC-App
//
//  Created by Frans Lourens on 2014/04/09.
//  Copyright (c) 2014 automytest. All rights reserved.
//

#import "SettingsViewController.h"
#import <Parse/Parse.h>

#import "GeoPointAnnotation.h"
#import "GeoQueryAnnotation.h"
#import "SPGooglePlacesAutocompleteQuery.h"
#import "SPGooglePlacesAutocompletePlace.h"
#import "ViewController.h"

enum PinAnnotationTypeTag {
    PinAnnotationTypeTagGeoPoint = 0,
    PinAnnotationTypeTagGeoQuery = 1
};

@interface SettingsViewController ()

@end

@implementation SettingsViewController
@synthesize mapView;
@synthesize locationManager = _locationManager;
@synthesize beginTime,beginDatePicker = _beginDatePicker,beginDatePikrToolbar = _beginDatePikrToolbar,beginDateFormatter=_beginDateFormatter;
@synthesize endTime,endDatePicker = _endDatePicker,endDatePikrToolbar = _endDatePikrToolbar,endDateFormatter=_endDateFormatter;

UIDatePicker *beginDatePicker;
UIDatePicker *endDatePicker;
NSDate *begin;
NSDate *end;
UIPickerView *gpsPicker;
NSString *gpsText;
BOOL updateLocation;
BOOL compare;
BOOL done;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        searchQuery = [[SPGooglePlacesAutocompleteQuery alloc] init];
        searchQuery.radius = radius;
        shouldBeginEditing = YES;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Home" style:UIBarButtonItemStylePlain target:self action:@selector(popToRootViewController:)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveSettings:)];

    self.navigationItem.hidesBackButton = YES;
    [self.searchBar sizeToFit];
    self.searchBar.delegate = self;
    [self beginPicker];
    [self endPicker];
    done = false;
    
    gpsChoices = [[NSArray alloc]
                       initWithObjects:
                       @"1 Hour", @"2 Hours", @"3 Hours", @"4 Hours", @"5 Hours",
                       @"6 Hours", @"7 Hours", @"8 Hours", @"9 Hours", @"10 Hours",
                       @"11 Hours", @"12 Hours", @"13 Hours", @"14 Hours", @"15 Hours",
                       @"16 Hours", @"17 Hours", @"18 Hours", @"19 Hours", @"20 Hours",
                       @"21 Hours", @"22 Hours", @"23 Hours", @"24 Hours",
                       nil];
    
    gpsPicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
    gpsPicker.delegate = self;
    gpsPicker.dataSource = self;
    [gpsPicker setShowsSelectionIndicator:YES];
    gps.inputView = gpsPicker;
    
    UIToolbar*  gpsToolbar = [[UIToolbar alloc]
                                   initWithFrame:CGRectMake(0, 0, 320, 40)];
    [gpsToolbar sizeToFit];
    
    NSMutableArray *gpsBarItems = [[NSMutableArray alloc] init];
    UIBarButtonItem *gpsFlexSpace = [[UIBarButtonItem alloc]
                                          initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                          target:self
                                          action:nil];
    
    [gpsBarItems addObject:gpsFlexSpace];
    UIBarButtonItem *gpsDoneBtn = [[UIBarButtonItem alloc]
                                        initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                        target:self
                                        action:@selector(gpsDoneClicked)];
    
    [gpsBarItems addObject:gpsDoneBtn];
    [gpsToolbar setItems:gpsBarItems animated:YES];
    
    gps.inputAccessoryView = gpsToolbar;

    PFQuery *query = [PFUser query];
    
    [query whereKey:@"username" equalTo:[[PFUser currentUser]username]];
    
    [query getObjectInBackgroundWithId:[PFUser currentUser].objectId block:^(PFObject *user, NSError *error) {
        if (!error) {
            PFGeoPoint *point = user[@"geolocation"];
            
            if (point != nil) {
            
                CLLocation *aUserLocation = [ [CLLocation alloc]
                                    initWithLatitude:point.latitude
                                    longitude:point.longitude];
     
                MKCoordinateRegion region;
                MKCoordinateSpan span;
            
                span.latitudeDelta = latitudeDelta;
                span.longitudeDelta = longitudeDelta;
            
                region.span = span;
                region.center = aUserLocation.coordinate;
            
                [self.mapView setRegion:region];
                [self.mapView removeAnnotations:self.mapView.annotations];
                [self.mapView removeOverlays:self.mapView.overlays];
                [self.mapView setShowsUserLocation:NO];
            
                GeoQueryAnnotation *annotation = [[GeoQueryAnnotation alloc]
                                              initWithCoordinate:aUserLocation.coordinate
                                              radius:self.radius];
            
                [self.mapView addAnnotation:annotation];
                [self setAddress:aUserLocation];
            }
            else {
                [self.locationManager startUpdatingLocation];
            }
        } else {
            if ( error.code == 101 ) {
                [self.locationManager startUpdatingLocation];
            }
            else {
                [self.locationManager startUpdatingLocation];
            }
            //self.searchDisplayController.searchBar.placeholder = @"My Location";
        }
    }];
    
    PFQuery *weekQuery = [PFQuery queryWithClassName:@"notification"];
    [weekQuery whereKey:@"user" equalTo:[PFUser currentUser]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDateFormatter *textDateFormatter = [[NSDateFormatter alloc] init];
    [textDateFormatter setDateFormat:@"hh:mm a"];
    
    [weekQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        //NSString *gpsHours = object[@"gpsHours"];
        //gps.text = gpsHours;
        //gpsText = gpsHours;
        NSLog(@"Begin Time : %@, End Time : %@", object[@"beginTime"], object[@"endTime"]);
        NSDate *beginString = [dateFormatter dateFromString:object[@"beginTime"]];
        NSDate *endString = [dateFormatter dateFromString:object[@"endTime"]];
    
        if (object[@"beginTime"] != nil) {
            self.beginTime.text = [[textDateFormatter stringFromDate:beginString] uppercaseString];
            _beginDatePicker.date = beginString;
            begin = beginString;
        }
        
        if (object[@"endTime"] != nil) {
            self.endTime.text = [[textDateFormatter stringFromDate:endString] uppercaseString];
            _endDatePicker.date = endString;
            end = endString;
        }
        
        self.monday.on = [object[@"Monday"] boolValue];
        self.tuesday.on = [object[@"Tuesday"] boolValue];
        self.wednesday.on = [object[@"Wednesday"] boolValue];
        self.thursday.on = [object[@"Thursday"] boolValue];
        self.friday.on = [object[@"Friday"] boolValue];
        self.saturday.on = [object[@"Saturday"] boolValue];
        self.sunday.on = [object[@"Sunday"] boolValue];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)cancelBeginDatePkrBtnActn{
    
    [self.beginTime resignFirstResponder];
}

-(void)doneBeginDatePkrBtnActn
{
    if (_beginDatePicker.date == [NSDate date])
    {
        NSDateFormatter *dateFormatter;
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"hh:mm a"];
        self.beginTime.text = [[NSString stringWithFormat:@"%@",
        [dateFormatter stringFromDate:begin]] uppercaseString];
        begin = _beginDatePicker.date;
    }
    else
    {
        NSDateFormatter *dateFormatter;
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"hh:mm a"];
        self.beginTime.text = [[NSString stringWithFormat:@"%@",
                               [dateFormatter stringFromDate:_beginDatePicker.date]] uppercaseString];
        begin = _beginDatePicker.date;
    }
    
    [self.beginTime resignFirstResponder];
}

-(void)cancelEndDatePkrBtnActn{
    
    [self.endTime resignFirstResponder];
}

-(void)doneEndDatePkrBtnActn
{
    if (_endDatePicker.date == [NSDate date])
    {
        NSDateFormatter *dateFormatter;
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"hh:mm a"];
        self.endTime.text = [[NSString stringWithFormat:@"%@",
        [dateFormatter stringFromDate:end]] uppercaseString];
        end = _endDatePicker.date;
    }
    else
    {
        NSDateFormatter *dateFormatter;
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"hh:mm a"];
        self.endTime.text = [[NSString stringWithFormat:@"%@",
                             [dateFormatter stringFromDate:_endDatePicker.date]] uppercaseString];
        end = _endDatePicker.date;
    }
    
    [self.endTime resignFirstResponder];
}

-(void)gpsDoneClicked
{
    if (gpsText == nil)
    {
        gps.text = gpsChoices[0];
    }
    else {
        gps.text = gpsText;
    }
    
    [gps resignFirstResponder];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    return gpsChoices.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [gpsChoices objectAtIndex:row];
}
- (void) pickerView:(UIPickerView *)pickerView
       didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    gpsText = (NSString *)[gpsChoices objectAtIndex:row];
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [searchResultPlaces count];
}

- (SPGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSIndexPath *)indexPath {
    return searchResultPlaces[indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"SPGooglePlacesAutocompleteCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"GillSans" size:16.0];
    cell.textLabel.text = [self placeAtIndexPath:indexPath].name;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.searchDisplayController setActive:NO animated:YES];
    SPGooglePlacesAutocompletePlace *place = [self placeAtIndexPath:indexPath];
    
    [place resolveToPlacemark:^(CLPlacemark *placemark, NSString *addressString, NSError *error) {
        
        if (error) {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Could not map selected Place"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil, nil];
            [alert show];
        } else if (placemark) {
            [self recenterMapToPlacemark:placemark];
            [self dismissSearchControllerWhileNotStayingActive];
            [self.searchDisplayController.searchResultsTableView deselectRowAtIndexPath:indexPath animated:NO];
            [self setAddress:placemark.location];
        }
    }];
    

}

#pragma mark -
#pragma mark UISearchDisplayDelegate

- (void)handleSearchForSearchString:(NSString *)searchString {
    searchQuery.location = self.mapView.userLocation.coordinate;
    searchQuery.input = searchString;
    [searchQuery fetchPlaces:^(NSArray *places, NSError *error) {
        if (error) {
            SPPresentAlertViewWithErrorAndTitle(error, @"Could not fetch Places");
        } else {
            searchResultPlaces = places;
            [self.searchDisplayController.searchResultsTableView reloadData];
        }
    }];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString {
    [self handleSearchForSearchString:searchString];
    return YES;
}

#pragma mark -
#pragma mark UISearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
    if (![searchBar isFirstResponder]) {
        shouldBeginEditing = NO;
        [self.mapView removeAnnotation:selectedPlaceAnnotation];
    }
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
    if (shouldBeginEditing) {
        [self.searchDisplayController setActive:YES animated:NO];
        self.searchDisplayController.searchBar.text = nil;
        searchBar.delegate = (id <UISearchBarDelegate>)self.searchDisplayController;
        [self.searchDisplayController.searchBar setShowsCancelButton:YES animated:NO];
    }
    BOOL boolToReturn = shouldBeginEditing;
    shouldBeginEditing = YES;
    return boolToReturn;
}

- (void)dismissSearchControllerWhileNotStayingActive {
    [self.searchDisplayController.searchBar setShowsCancelButton:NO animated:NO];
    [self.searchDisplayController.searchBar resignFirstResponder];
    [self.searchDisplayController setActive:NO animated:NO];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void) searchDisplayControllerDidBeginSearch:(UISearchDisplayController
                                                *)controller {
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void) searchDisplayControllerWillEndSearch:(UISearchDisplayController
                                               *)controller {
    [self.searchBar resignFirstResponder];
    self.searchBar.delegate = self;
    [self.searchDisplayController setActive:NO animated:NO];
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapViewIn
            viewForAnnotation:(id <MKAnnotation>)annotation
{
    if (    mapViewIn != self.mapView ||
        [annotation isKindOfClass:[MKUserLocation class]]
        )
    {
        return nil;
    }
    
    static NSString *GeoQueryAnnotationIdentifier = @"SPGooglePlacesAutocompleteAnnotation";
    
    if ([annotation isKindOfClass:[GeoQueryAnnotation class]]) {
        MKPinAnnotationView *annotationView =
        (MKPinAnnotationView *)[mapView
                                dequeueReusableAnnotationViewWithIdentifier:GeoQueryAnnotationIdentifier];
        
        if (!annotationView) {
            annotationView = [[MKPinAnnotationView alloc]
                              initWithAnnotation:annotation
                              reuseIdentifier:GeoQueryAnnotationIdentifier];
            annotationView.tag = PinAnnotationTypeTagGeoQuery;
            annotationView.canShowCallout = NO;
            annotationView.pinColor = MKPinAnnotationColorRed;
            annotationView.animatesDrop = NO;
            annotationView.draggable = YES;
        }
        
        return annotationView;
    }
    
    return nil;
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    [self.mapView selectAnnotation:selectedPlaceAnnotation animated:NO];
}

- (void)mapView:(MKMapView *)mapView
 annotationView:(MKAnnotationView *)annotationView
didChangeDragState:(MKAnnotationViewDragState)newState
   fromOldState:(MKAnnotationViewDragState)oldState
{
    if (newState == MKAnnotationViewDragStateEnding) {
        CLLocationCoordinate2D droppedAt = annotationView.annotation.coordinate;
        
        
        CLLocation *location = [ [CLLocation alloc]
                                initWithLatitude:droppedAt.latitude
                                longitude:droppedAt.longitude];
        
        [self setInitialLocation:location];
        [self setAddress:location];
        [self.mapView removeOverlays:self.mapView.overlays];
    }
}

- (void)mapView:(MKMapView *)mapView
didUpdateUserLocation:(MKUserLocation *)userLocation
{
   self.mapView.region = MKCoordinateRegionMake(userLocation.location.coordinate,
                                                MKCoordinateSpanMake(latitudeDelta, longitudeDelta));
   [self setInitialLocation:userLocation.location];
   [self configureOverlay];
}

- (void)recenterMapToPlacemark:(CLPlacemark *)placemark {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    
    span.latitudeDelta = 0.00;
    span.longitudeDelta = 0.00;
    
    region.span = span;
    region.center = placemark.location.coordinate;
    
    [self.mapView setRegion:region];
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView removeOverlays:self.mapView.overlays];
    [self.mapView setShowsUserLocation:NO];
    
    GeoQueryAnnotation *annotation = [[GeoQueryAnnotation alloc]
                                      initWithCoordinate:placemark.location.coordinate
                                      radius:self.radius];
    
    [self.mapView addAnnotation:annotation];
}

- (IBAction)recenterMapToUserLocation:(id)sender {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    
    span.latitudeDelta = 0.00;
    span.longitudeDelta = 0.00;
    
    region.span = span;
    region.center = self.mapView.userLocation.coordinate;
    
    [self.mapView setRegion:region];
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView removeOverlays:self.mapView.overlays];
    [self.mapView setShowsUserLocation:NO];
    [self setAddress:self.mapView.userLocation.location];
    
    GeoQueryAnnotation *annotation = [[GeoQueryAnnotation alloc]
                                      initWithCoordinate:self.mapView.userLocation.coordinate
                                      radius:self.radius];
    
    [self.mapView addAnnotation:annotation];
}

- (void)setInitialLocation:(CLLocation *)aLocation {
    self.location = aLocation;
    self.radius = radius;
}

- (void)setAddress:(CLLocation *)aLocation {
    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
    
    [geoCoder reverseGeocodeLocation:aLocation completionHandler:
     ^(NSArray *placemarks, NSError *error) {
         for (CLPlacemark * placemark in placemarks) {
             self.country = [placemark country];
             self.city = [placemark locality];
             self.latitude = aLocation.coordinate.latitude;
             self.longitude = aLocation.coordinate.longitude;
             
             NSString * address = [placemark name];
             self.searchDisplayController.searchBar.text = address;
         }
     }];
}

- (void)configureOverlay {
    if (self.location) {
        [self.mapView removeAnnotations:self.mapView.annotations];
        [self.mapView setShowsUserLocation:NO];
        [self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
        [self setAddress:self.location];
        
        GeoQueryAnnotation *annotation = [[GeoQueryAnnotation alloc]
                                          initWithCoordinate:self.location.coordinate
                                          radius:self.radius];
        [self.mapView addAnnotation:annotation];
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    if (newLocation != nil) {
        self.location = newLocation;
    }
    
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSString *errorType = (error.code == kCLErrorDenied) ? @"Access Denied" : @"Unknown Error";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error getting Location"message:errorType delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
    [alert show];
}

- (IBAction)saveSettings:(id)sender {
    if (    self.searchDisplayController.searchBar.text.length == 0 ||
            self.beginTime.text.length == 0 ||
            self.endTime.text.length == 0 //||
            //gps.text.length == 0
        ) {
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Please fill in all the missing fields!"
                              message:@""
                              delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else {
        NSString *message;
        
        switch ([_beginDatePicker.date compare:_endDatePicker.date]){
            case NSOrderedAscending:
                compare = true;
                NSLog(@"NSOrderedAscending %@, %@", begin, end);
                break;
            case NSOrderedSame:
                compare = false;
                message = @"Your Start Time is equal to your End Time!";
                NSLog(@"NSOrderedSame %@, %@", begin, end);
                break;
            case NSOrderedDescending:
                compare = false;
                message = @"Your Start Time is bigger than your End Time!";
                NSLog(@"NSOrderedDescending %@, %@", begin, end);
                break;
            }
        
        if (!compare) {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:message
                                  message:@""
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
            //NSLog(@"Begin Time : %@, End Time : %@", begin, end);
        } else {
        UIAlertView *alert = [[UIAlertView alloc]
                            initWithTitle:@"Successfully Saved Settings!"
                            message:@""
                            delegate:self
                            cancelButtonTitle:@"OK"
                            otherButtonTitles:nil];
        PFQuery *query = [PFUser query];
        
        [query whereKey:@"username" equalTo:[[PFUser currentUser]username]];
        
        [query getObjectInBackgroundWithId:[PFUser currentUser].objectId block:^(PFObject *user, NSError *error) {
            if (!error) {
                PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLatitude:self.latitude longitude:self.longitude];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"HH:mm"];
                
                NSString *beginString = [dateFormatter stringFromDate:_beginDatePicker.date];
                NSString *endString = [dateFormatter stringFromDate:_endDatePicker.date];
                
                NSNumber * mo = [NSNumber numberWithBool:self.monday.isOn];
                NSNumber * tu = [NSNumber numberWithBool:self.tuesday.isOn];
                NSNumber * we = [NSNumber numberWithBool:self.wednesday.isOn];
                NSNumber * thu = [NSNumber numberWithBool:self.thursday.isOn];
                NSNumber * fri = [NSNumber numberWithBool:self.friday.isOn];
                NSNumber * sa = [NSNumber numberWithBool:self.saturday.isOn];
                NSNumber * sun = [NSNumber numberWithBool:self.sunday.isOn];
                
                PFQuery *weekQuery = [PFQuery queryWithClassName:@"notification"];
                [weekQuery whereKey:@"user" equalTo:[PFUser currentUser]];
                
                [weekQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                    if (!error) {
                        object[@"Monday"] = mo;
                        object[@"Tuesday"] = tu;
                        object[@"Wednesday"] = we;
                        object[@"Thursday"] = thu;
                        object[@"Friday"] = fri;
                        object[@"Saturday"] = sa;
                        object[@"Sunday"] = sun;
                        object[@"beginTime"] = beginString;
                        object[@"endTime"] = endString;
                        //object[@"gpsHours"] = gpsText;
                        [object saveInBackground];
                    }
                    else if (error.code == 101) {
                        PFObject *weekObject = [PFObject objectWithClassName:@"notification"];
                        weekObject[@"Monday"] = mo;
                        weekObject[@"Tuesday"] = tu;
                        weekObject[@"Wednesday"] = we;
                        weekObject[@"Thursday"] = thu;
                        weekObject[@"Friday"] = fri;
                        weekObject[@"Saturday"] = sa;
                        weekObject[@"Sunday"] = sun;
                        weekObject[@"user"] = [PFUser currentUser];
                        weekObject[@"beginTime"] = beginString;
                        weekObject[@"endTime"] = endString;
                        //weekObject[@"gpsHours"] = gpsText;
                        [weekObject saveInBackground];
                    }
                }];
                
                [user setObject:geoPoint forKey:@"geolocation"];
                [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    
                    if (error) {
                        NSLog(@"Couldn't save!");
                        NSLog(@"%@", error);
                        done = false;
                        return;
                    }
        
                    if (succeeded) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                           [alert show];
                        });
                        done = true;
 
                    }
                    else {
                        NSLog(@"Failed to save.");
                        done = false;
                    }
                }];
           }
        }];
            
            if (done) {
                //[alert show];
            }
    }
    }
}

-(void)beginPicker {
    NSDate *now = [NSDate date];
        NSDateComponents *nowComponents = [[NSCalendar currentCalendar] components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:now];

    [nowComponents setHour:0];
    [nowComponents setMinute:1];
    
    NSDate *todayAtMidnight = [[NSCalendar currentCalendar] dateFromComponents:nowComponents];
    
    begin = todayAtMidnight;

    
    UIButton *cancelBeginToolbarBtnCstm = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    cancelBeginToolbarBtnCstm.frame=CGRectMake(0, 0, 80, 40);
    
    [cancelBeginToolbarBtnCstm setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelBeginToolbarBtnCstm addTarget:self action:@selector(cancelBeginDatePkrBtnActn) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelBeginToolbarBtn=[[UIBarButtonItem alloc] initWithCustomView:cancelBeginToolbarBtnCstm];
    
    UIBarButtonItem *beginFixedSpaceToolbarITem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [beginFixedSpaceToolbarITem setWidth:140];
    
    UIButton *doneBeginToolbarBtnCstm = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    doneBeginToolbarBtnCstm.frame=CGRectMake(0, 0, 80, 40);
    [doneBeginToolbarBtnCstm setTitle:@"Done" forState:UIControlStateNormal];
    [doneBeginToolbarBtnCstm addTarget:self action:@selector(doneBeginDatePkrBtnActn) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *doneToolbarBtn=[[UIBarButtonItem alloc] initWithCustomView:doneBeginToolbarBtnCstm];
    
    NSMutableArray *beginToolbarinptArray=[[NSMutableArray alloc]init];
    [beginToolbarinptArray addObject:cancelBeginToolbarBtn];
    [beginToolbarinptArray addObject:beginFixedSpaceToolbarITem];
    [beginToolbarinptArray addObject:doneToolbarBtn];
    
    _beginDatePikrToolbar =[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,320,40)];
    [_beginDatePikrToolbar setItems:beginToolbarinptArray animated:NO];
    
    _beginDatePicker=[[UIDatePicker alloc] initWithFrame:CGRectMake(0,40,320,220)];
    _beginDatePicker.datePickerMode=UIDatePickerModeTime;
    _beginDatePicker.date = todayAtMidnight;
    [beginDatePicker setDatePickerMode:UIDatePickerModeTime];
    [beginDatePicker setDate:todayAtMidnight];
    
    UIView *beginDatePickWithToolbarView = [[UIView alloc] initWithFrame:CGRectMake(0,220,320,260)];
    [beginDatePickWithToolbarView addSubview:_beginDatePikrToolbar];
    [beginDatePickWithToolbarView addSubview:_beginDatePicker];
    
    [self.beginTime setInputView:beginDatePickWithToolbarView];
    
    NSDateFormatter *todayFormatter;
    todayFormatter = [[NSDateFormatter alloc] init];
    [todayFormatter setDateFormat:@"hh:mm a"];
    
    
    if (self.beginTime.text.length == 0) {
        self.beginTime.text = [[NSString stringWithFormat:@"%@",
                           [todayFormatter stringFromDate:todayAtMidnight]] uppercaseString];
    }
    
}

-(void)endPicker {
    NSDate *later = [NSDate date];
    
    NSDateComponents *laterComponents = [[NSCalendar currentCalendar] components:
                                         (NSEraCalendarUnit | NSYearCalendarUnit |
                                          NSMonthCalendarUnit | NSDayCalendarUnit)
                                        fromDate:later];

    [laterComponents setHour:23];
    [laterComponents setMinute:59];
    
    NSDate *laterAtMidnight = [[NSCalendar currentCalendar] dateFromComponents:laterComponents];
    end = laterAtMidnight;
    
    UIButton *cancelEndToolbarBtnCstm = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    cancelEndToolbarBtnCstm.frame=CGRectMake(0, 0, 80, 40);
    
    [cancelEndToolbarBtnCstm setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelEndToolbarBtnCstm addTarget:self action:@selector(cancelEndDatePkrBtnActn)
                                        forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelEndToolbarBtn=[[UIBarButtonItem alloc] initWithCustomView:cancelEndToolbarBtnCstm];
    
    UIBarButtonItem *endFixedSpaceToolbarITem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [endFixedSpaceToolbarITem setWidth:140];
    
    UIButton *doneEndToolbarBtnCstm = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    doneEndToolbarBtnCstm.frame=CGRectMake(0, 0, 80, 40);
    [doneEndToolbarBtnCstm setTitle:@"Done" forState:UIControlStateNormal];
    [doneEndToolbarBtnCstm addTarget:self action:@selector(doneEndDatePkrBtnActn) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *endToolbarBtn=[[UIBarButtonItem alloc] initWithCustomView:doneEndToolbarBtnCstm];
    
    NSMutableArray *endToolbarinptArray=[[NSMutableArray alloc]init];
    [endToolbarinptArray addObject:cancelEndToolbarBtn];
    [endToolbarinptArray addObject:endFixedSpaceToolbarITem];
    [endToolbarinptArray addObject:endToolbarBtn];
    
    _endDatePikrToolbar =[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,320,40)];
    [_endDatePikrToolbar setItems:endToolbarinptArray animated:NO];
    
    _endDatePicker=[[UIDatePicker alloc] initWithFrame:CGRectMake(0,40,320,220)];
    
    _endDatePicker.datePickerMode=UIDatePickerModeTime;
    _endDatePicker.date = laterAtMidnight;
    UIView *endDatePickWithToolbarView = [[UIView alloc] initWithFrame:CGRectMake(0,220,320,260)];
    [endDatePickWithToolbarView addSubview:_endDatePikrToolbar];
    [endDatePickWithToolbarView addSubview:_endDatePicker];
    
    [self.endTime setInputView:endDatePickWithToolbarView];
    
    NSDateFormatter *todayFormatter;
    todayFormatter = [[NSDateFormatter alloc] init];
    [todayFormatter setDateFormat:@"hh:mm a"];
    
    if (self.endTime.text.length == 0) {
        self.endTime.text = [[NSString stringWithFormat:@"%@",
                         [todayFormatter stringFromDate:laterAtMidnight]] uppercaseString];
    }
}

-(void)popToRootViewController:(id)sender
{
    PFQuery *query = [PFQuery queryWithClassName:@"_User"];
    
    // Retrieve the object by id
    [query getObjectInBackgroundWithId:[PFUser currentUser].objectId block:^(PFObject *user, NSError *error) {
        NSLog(@"Username : %@", user[@"username"]);
        if(user[@"geolocation"] == nil) {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Please save your location before you continue!"
                                  message:@""
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
        else{
            UIViewController *myController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
            [self.navigationController pushViewController:myController animated:NO];
        }
        
    }];

}

@end