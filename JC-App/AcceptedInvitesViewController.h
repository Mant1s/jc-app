//
//  AcceptedInvitesViewController.h
//  JC-App
//
//  Created by Frans Lourens on 2014/03/17.
//  Copyright (c) 2014 automytest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <Parse/Parse.h>

@interface AcceptedInvitesViewController : PFQueryTableViewController <
UITableViewDelegate,
UITableViewDataSource
>
@end
