#import <UIKit/UIKit.h>

#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CircleOverlay.h"

@class SPGooglePlacesAutocompleteQuery;

@interface LocationViewController : UIViewController
<
    UITextFieldDelegate,
    UITextViewDelegate,
    UITableViewDelegate,
    UITableViewDataSource,
    UISearchDisplayDelegate,
    UISearchBarDelegate,
    MKMapViewDelegate,
    CLLocationManagerDelegate,
    UIPickerViewDataSource,
    UIPickerViewDelegate
>
{
    IBOutlet UITextField *distanceField;
    NSArray *distanceChoices;
    NSArray *searchResultPlaces;
    NSArray *people;
    SPGooglePlacesAutocompleteQuery *searchQuery;
    MKPointAnnotation *selectedPlaceAnnotation;
    BOOL shouldBeginEditing;
}

@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;

@property (nonatomic, retain) CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, assign) CLLocationDistance radius;
@property (nonatomic, strong) CircleOverlay *targetOverlay;

@property (weak, nonatomic) IBOutlet UITextView *meetingSpotField;
@property (nonatomic, strong) NSString *country;
//@property (nonatomic, strong) NSArray *people;
@property (nonatomic, strong) NSString *city;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (nonatomic) double latDelta;
@property (nonatomic) double lonDelta;
@property (weak, nonatomic) IBOutlet UILabel *warning;
@property (weak, nonatomic) IBOutlet UITextField *totalUsers;


- (IBAction)locationBackButton:(id)sender;
@end
