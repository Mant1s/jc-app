#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *inviteOther;
@property (weak, nonatomic) IBOutlet UIButton *inviteSent;
@property (weak, nonatomic) IBOutlet UIButton *inviteAccept;
@property (weak, nonatomic) IBOutlet UIButton *inviteNotAccept;
@property (weak, nonatomic) IBOutlet UIButton *settingsLabel;
- (IBAction)whatButton:(id)sender;
@end
