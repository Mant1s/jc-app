//
//  AboutViewController.h
//  JC-App
//
//  Created by Frans Lourens on 2014/05/26.
//  Copyright (c) 2014 automytest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController
- (IBAction)email:(id)sender;
- (IBAction)twitter:(id)sender;
- (IBAction)facebook:(id)sender;
- (IBAction)web:(id)sender;
@end
