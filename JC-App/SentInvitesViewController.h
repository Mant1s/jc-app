//
//  SentInvitesViewController.h
//  JC-App
//
//  Created by Frans Lourens on 2014/04/25.
//  Copyright (c) 2014 automytest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface SentInvitesViewController : PFQueryTableViewController <
UITableViewDelegate,
UITableViewDataSource
>
@property (nonatomic,strong) PFObject *invite;
@end
