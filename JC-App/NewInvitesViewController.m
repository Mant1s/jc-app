#import "AcceptedInvitesViewController.h"
#import "NewInvitesViewController.h"
#import "UpdatesTableViewCell.h"
#import "ViewController.h"
#import "ViewInviteViewController.h"

@interface NewInvitesViewController ()
@property (nonatomic, strong) NSArray *invites;
@end

@implementation NewInvitesViewController

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        
        self.pullToRefreshEnabled = YES;
        self.paginationEnabled = YES;
        self.objectsPerPage = 5;
    }
    return self;
}

- (PFQuery *)queryForTable {
    
    NSDate *now = [NSDate date];
    
    PFQuery *query = [PFQuery queryWithClassName:@"invite_attendance"];
    [query whereKey:@"attendee" equalTo:[PFUser currentUser]];
    [query whereKey:@"attending" equalTo:[NSNumber numberWithBool:NO]];
    [query whereKey:@"expire" greaterThan:now];
    [query includeKey:@"invite.location"];
    
    if ([self.objects count] == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    
    [query orderByAscending:@"expire"];
    
    return query;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Home" style:UIBarButtonItemStyleBordered target:self action:@selector(back)];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = item;
}

-(void)back {
    UIViewController *myController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
    [self.navigationController pushViewController:myController animated:NO];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
         object:(PFObject *)object {
    
    static NSString *simpleTableIdentifier = @"inviteViewCell";
    
    UpdatesTableViewCell *cell = (UpdatesTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"UpdatesListView" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    PFObject *invitation = [object objectForKey:@"invite"];
    PFObject *location = [invitation objectForKey:@"location"];
    
    NSDate *now = [invitation objectForKey:@"date"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSString *datetime = [formatter stringFromDate:now];
    //cell.locationLabel.enabled = NO;
    cell.descriptionLabel.backgroundColor = (indexPath.row%2)?[UIColor clearColor]:[UIColor lightGrayColor];
    cell.descriptionLabel.editable = NO;
    cell.titleLabel.text = [invitation objectForKey:@"reason"];
    cell.descriptionLabel.text = [invitation objectForKey:@"topic"];
    cell.locationLabel.text = [NSString stringWithFormat:@"Near: %@, %@",
                               [location objectForKey:@"address"],
                               [location objectForKey:@"city"]];
    cell.exactMeetingPointLabel.text = [location objectForKey:@"meetingPoint"];
    cell.dateLabel.text = datetime;
    cell.timelabel.text = [invitation objectForKey:@"duration"];
    
    cell.backgroundColor = (indexPath.row%2)?[UIColor clearColor]:[UIColor lightGrayColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    PFObject *selectedObject = [self objectAtIndexPath:indexPath];

    UIStoryboard *storyboard = self.storyboard;

    ViewInviteViewController *vc = [storyboard
                                    instantiateViewControllerWithIdentifier:@"ViewInvite"];
    
    PFObject *invite = [selectedObject objectForKey:@"invite"];
 
    [vc setInvite:invite];

    [self.navigationController pushViewController:vc animated:YES];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}
@end