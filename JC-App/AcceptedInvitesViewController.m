#import "AcceptedInvitesViewController.h"
#import <Parse/Parse.h>
#import "UpdatesTableViewCell.h"
#import "ViewInviteViewController.h"

@interface AcceptedInvitesViewController ()

@end
NSDateFormatter *dateFormatter;

@implementation AcceptedInvitesViewController
{
    
}

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        self.pullToRefreshEnabled = YES;
        self.paginationEnabled = YES;
        self.objectsPerPage = 5;
    }
    return self;
}

- (PFQuery *)queryForTable {
    NSDate *now = [NSDate date];
    
    PFQuery *query = [PFQuery queryWithClassName:@"invite_attendance"];
    [query whereKey:@"attendee" equalTo:[PFUser currentUser]];
    [query whereKey:@"attending" equalTo:[NSNumber numberWithBool:TRUE]];
    [query whereKey:@"expire" greaterThan:now];
    [query includeKey:@"invite.location"];
    [query includeKey:@"invite.user"];
    
    if ([self.objects count] == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    
    [query orderByAscending:@"expire"];
    
    return query;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
                        object:(PFObject *)object {
    
    static NSString *simpleTableIdentifier = @"inviteViewCell";
    
    UpdatesTableViewCell *cell = (UpdatesTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"UpdatesListView" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    PFObject *invite = [object objectForKey:@"invite"];
    PFObject *location = [invite objectForKey:@"location"];
    
    NSDate *now = [invite objectForKey:@"date"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSString *datetime = [formatter stringFromDate:now];
    
    cell.descriptionLabel.backgroundColor = (indexPath.row%2)?[UIColor clearColor]:[UIColor lightGrayColor];
    cell.descriptionLabel.editable = NO;
    cell.titleLabel.text = [invite objectForKey:@"reason"];
    cell.descriptionLabel.text = [invite objectForKey:@"topic"];
    cell.locationLabel.text = [NSString stringWithFormat:@"Near:  %@, %@",
                               [location objectForKey:@"address"],
                               [location objectForKey:@"city"]];
    //cell.locationLabel.enabled = NO;
    cell.exactMeetingPointLabel.text = [location objectForKey:@"meetingPoint"];
    cell.dateLabel.text = datetime;
    cell.timelabel.text = [invite objectForKey:@"duration"];
    
    cell.backgroundColor = (indexPath.row%2)?[UIColor clearColor]:[UIColor lightGrayColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    PFObject *selectedObject = [self objectAtIndexPath:indexPath];
    
    //the storyboard
    UIStoryboard *storyboard = self.storyboard;
    
    //the detail controller
    ViewInviteViewController *vc = [storyboard
                                    instantiateViewControllerWithIdentifier:@"ViewInvite"];
    
    PFObject *invite = [selectedObject objectForKey:@"invite"];
    //set the product
    [vc setInvite:invite];
    //NSLog(@"Selected Object : %@",invite.objectId);
    //Push to detail View
    [self.navigationController pushViewController:vc animated:YES];
    
}

@end